# Bärbelfish

## Systemvoraussetzungen

OXID eShop >4.6


## Installation

Erstellen Sie in der Shop-Root einen Ordner namens 'fish' und legen Sie die Dateien dieses Repositories hinein.
Bärbelfish erkennt automatisch das unterstützte Shopsystem. 

Die notwendige Bibliotheken (Dependencies) können Sie mit Composer installieren. 
Führen Sie hierfür den Befehl - php composer.phar install - über das Terminal in dem fish-Verzeichnis aus.


### Sicherheitshinweis

Es empfiehlt sich, einen htaccess-Schutz einzurichten, um Bärbelfish vor Fremdzugriffen zu schützen.