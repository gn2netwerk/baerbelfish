<?php
namespace gn2\Baerbelfish\Core;

class Translation
{
    private $_internalId;
    private $_externalId;
    private $_objectTitle;
    private $_translationStatus;

    public function setInternalId($sInternal) {
        $this->_internalId = $sInternal;
    }

    public function getInternalId() {
        return $this->_internalId;
    }

    public function setExternalId($sExternal) {
        $this->_externalId = $sExternal;
    }

    public function getExternalId() {
        return $this->_externalId;
    }

    public function setObjectTitle($sTitle) {
        $this->_objectTitle = $sTitle;
    }

    public function getObjectTitle() {
        return $this->_objectTitle;
    }

    public function setTranslationStatus($status) {
        $this->_translationStatus = $status;
    }

    public function getTranslationStatus() {
        return $this->_translationStatus;
    }





}