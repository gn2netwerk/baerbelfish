<?php
namespace gn2\Baerbelfish\Core;

class Filter
{
    private $_title;
    private $_type;
    private $_name;
    private $_value;
    private $_checkedValue;
    private $_selection;
    private $_info;


    public function getTitle()
    {
        return $this->_title;
    }

    public function setTitle($sTitle)
    {
        $this->_title = $sTitle;
    }

    public function getType()
    {
        return $this->_type;
    }

    public function setType($sType)
    {
        $this->_type = $sType;
    }

    public function getName()
    {
        return $this->_name;
    }

    public function setName($sName)
    {
        $this->_name = $sName;
    }

    public function getValue()
    {
        return $this->_value;
    }

    public function setValue($value)
    {
        $this->_value = $value;
    }

    public function getCheckedValue()
    {
        return $this->_checkedValue;
    }

    public function setCheckedValue($value)
    {
        $this->_checkedValue = $value;
    }

    public function getSelection()
    {
        return $this->_selection;
    }

    public function setSelection($selection)
    {
        $this->_selection = $selection;
    }

    public function getInfoText()
    {
        return $this->_info;
    }

    public function setInfoText($sInfo)
    {
        $this->_info = $sInfo;
    }
}