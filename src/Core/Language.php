<?php
namespace gn2\Baerbelfish\Core;

class Language
{
    private $_id, $_name, $_iso;

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->_id = $id;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->_id;
    }

    /**
     * @param mixed $iso
     */
    public function setIso($iso)
    {
        $this->_iso = $iso;
    }

    /**
     * @return mixed
     */
    public function getIso()
    {
        return $this->_iso;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->_name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->_name;
    }

}
?>
