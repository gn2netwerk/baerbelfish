<?php
namespace gn2\Baerbelfish\Core;

/**
 * Class Plugin
 * @package gn2\Translate
 */
/**
 * Class Plugin
 * @package gn2\Baerbelfish\Core
 */
/**
 * Class Plugin
 * @package gn2\Baerbelfish\Core
 */
abstract class Plugin
{
    /**
     * @return mixed
     */
    abstract public function getName();

    /**
     * @return mixed
     */
    abstract public function getId();

    /**
     * @return mixed
     */
    abstract public function isActive();

    /**
     * @return mixed
     */
    abstract public function getRows();


    /**
     * @return int
     */
    abstract public function getCount();

    /**
     * @return mixed
     */
    abstract protected function _getConfig();


    /**
     * @return mixed
     */
    abstract public function getFilter();

    /**
     * @return mixed
     */
    abstract public function getListModalTitle();

    /**
     * @return mixed
     */
    abstract public function addSpecialHeader();

    /**
     * @param $newFilter
     * @return mixed
     */
    abstract public function setFilter($newFilter);


    /**
     * @param $oArticle
     * @return mixed
     */
    abstract public function getObjectInternalId($oArticle);

    /**
     * @param $newShopID
     * @return mixed
     */
    abstract public function setShopID ($newShopID);

    /**
     * @return mixed
     */
    abstract public function getShopID ();

    /**
     * @param $newLang
     * @return mixed
     */
    abstract public function setSourceLanguage ($newLang);

    /**
     * @return mixed
     */
    abstract public function getSourceLanguage ();

    /**
     * @param $newLang
     * @return mixed
     */
    abstract public function setDestLanguage ($newLang);

    /**
     * @return mixed
     */
    abstract public function getDestLanguage ();


    /**
     * @param $searchObjectId
     * @param $sLangSrc
     * @param $sLangDest
     * @return mixed
     */
    abstract public function getCompleteObject($searchObjectId, $sLangSrc, $sLangDest);


    /**
     * @param $oArticle
     * @return mixed
     */
    abstract public function getObjectId($oArticle);

    /**
     * @param $oArticle
     * @return mixed
     */
    abstract public function getObjectTitle($oArticle);

    /**
     * @return mixed
     */
    abstract public function getNaviIcon();

}
?>
