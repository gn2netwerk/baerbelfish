$(document).ready(function() {
    $("#bfObjectList button.btChange").click( function() {
        var functionActive = "editobj";
        var strPluginId = $("#pluginactive").val();
        var strShopID = $("#ShopIDActive").val();
        var strLanguageSource = $("#langSourceActive").val();
        var strLanguageDest = $("#langDestActive").val();
        var sUrl = "?function=" + functionActive +
        "&plugin=" + strPluginId +
        "&shopID=" + strShopID +
        "&langSource=" + strLanguageSource +
        "&langDest=" + strLanguageDest+
        "&objId=" + $(this).attr("data-objectId");



        $("#myEditObject .modal-body").load(sUrl, function() {
            $("#myEditObject").modal("show");

            var editObjSubmitButton = $('#editObjForm .btsubmit'),
                stickySubmitButton = $('.editObjFormSubmit');

            if (editObjSubmitButton.length > 0) {
                // submitbutton form bottom
                editObjSubmitButton.click(function(){
                    SendEditObjForm();
                });

                // sticky button trigger event
                if (stickySubmitButton.length > 0) {
                    stickySubmitButton.click(function(){
                        editObjSubmitButton.trigger('click');
                    });
                }
            }
        });

        //$('#myEditObject').on('hide.bs.modal', function (e) {
            // location.reload();
        //})
    } );

    $("#btAllProperties").click( function() {
        var functionActive = "allProp";
        var strPluginId = $("#pluginactive").val();
        var nShopID = parseInt($("#shopID").val());
        if (isNaN(nShopID) ) nShopID = 0;
        var strLanguageSource = $("#langSourceActive").val();
        var strLanguageDest = $("#langDestActive").val();


        var strMessText = 'Einen Moment Geduld bitte, die Instavariants-Produkte werden ausgelesen und die Eigenschaften aufgelistet!';
        if (nShopID == 0) {
            strMessText += '<br>';
            strMessText += 'Es wurde kein Shop ausgewählt, darum wird der Standardshop verwendet!';
            nShopID = 1;
        }
        showUserMessage(strMessText);


        var sUrl = "?function=" + functionActive +
            "&plugin=" + strPluginId +
            "&shopID=" + nShopID +
            "&langSource=" + strLanguageSource +
            "&langDest=" + strLanguageDest +
            "&curFunc=showProperties";
        $("#myEditObject .modal-body").html("...");
        $("#myEditObject .modal-body").load(sUrl, function() {
            $("#myEditObject").modal("show");
            $('#editObjForm .btsubmit').click(function () {
                showUserMessage('Einen Moment Geduld bitte, die Instavariants-Produkte werden gespeichert!');
                SendEditAllForm();
            } );
        });

    });

    $("#btAllComponents").click( function() {
        var functionActive = "allComp";
        var strPluginId = $("#pluginactive").val();
        var nShopID = parseInt($("#shopID").val());
        if (isNaN(nShopID) ) nShopID = 0;
        var strLanguageSource = $("#langSourceActive").val();
        var strLanguageDest = $("#langDestActive").val();


        var strMessText = 'Einen Moment Geduld bitte, die Instavariants-Produkte werden ausgelesen und die Komponenten aufgelistet!';
        if (nShopID == 0) {
            strMessText += '<br>';
            strMessText += 'Es wurde kein Shop ausgewählt, darum wird der Standardshop verwendet!';
            nShopID = 1;
        }
        showUserMessage(strMessText);


        var sUrl = "?function=" + functionActive +
            "&plugin=" + strPluginId +
            "&shopID=" + nShopID +
            "&langSource=" + strLanguageSource +
            "&langDest=" + strLanguageDest +
            "&curFunc=showComponents";

        $("#myEditObject .modal-body").html("...");

        $("#myEditObject .modal-body").load(sUrl, function() {
            $("#myEditObject").modal("show");
            $('#editObjForm .btsubmit').click(function () {
                showUserMessage('Einen Moment Geduld bitte, die Instavariants-Produkte werden gespeichert!');
                SendEditAllForm();
            } );
        });

    });

    $("#btAllPropValues").click( function() {
        var functionActive = "allPropVal";
        var strPluginId = $("#pluginactive").val();
        var nShopID = parseInt($("#shopID").val());
        if (isNaN(nShopID) ) nShopID = 0;
        var strLanguageSource = $("#langSourceActive").val();
        var strLanguageDest = $("#langDestActive").val();


        var strMessText = 'Einen Moment Geduld bitte, die Instavariants-Produkte werden ausgelesen und die Werte aufgelistet!';
        if (nShopID == 0) {
            strMessText += '<br>';
            strMessText += 'Es wurde kein Shop ausgewählt, darum wird der Standardshop verwendet!';
            nShopID = 1;
        }
        showUserMessage(strMessText);


        var sUrl = "?function=" + functionActive +
            "&plugin=" + strPluginId +
            "&shopID=" + nShopID +
            "&langSource=" + strLanguageSource +
            "&langDest=" + strLanguageDest +
            "&curFunc=showPropValues";

        $("#myEditObject .modal-body").html("...");

        $("#myEditObject .modal-body").load(sUrl, function() {
            $("#myEditObject").modal("show");
            $('#editObjForm .btsubmit').click(function () {
                showUserMessage('Einen Moment Geduld bitte, die Instavariants-Produkte werden gespeichert!');
                SendEditAllForm();
            } );
        });

    });

    jQuery("#shopID").change(function(e) {
        $("#shopIDActive").val($(this).val())
        $("#formSelection").submit();
    });

    jQuery("#languageSource").change(function(e) {
        $("#langSourceActive").val($(this).val())
        $("#formSelection").submit();
    });

    jQuery("#languageDestination").change(function(e){
        $("#langDestActive").val($(this).val())
        $("#formSelection").submit();
    });

    jQuery('#menu').addClass('cbp-spmenu cbp-spmenu-vertical cbp-spmenu-left');
    jQuery('#menu .button').addClass('toggle-menu menu-left').jPushMenu();

    jQuery('.filterbutton').click(function(){
        var content = jQuery('.filtercontent');
        if (content.is(':visible')) {
            content.slideUp(500, function(){
                jQuery('.filterbutton').removeClass('open');
            });
            jQuery.cookie('bf_filter', 'closed');
        } else {
            content.slideDown(500, function(){
                jQuery('.filterbutton').addClass('open');
            });
            jQuery.cookie('bf_filter', 'open');
        }
    });

    if (jQuery.cookie('bf_filter') == 'open') {
        jQuery('.filtercontent').show();
        jQuery('.filterbutton').addClass('open');
    }

    if (jQuery('header.start').size() > 0) {
        jQuery('nav div.button').trigger('click');
    }

});

function SendEditObjForm() {
    var xForm = $("#editObjForm");
    var sURL = "index.php";

    $.ajax({
        url: sURL,
        data : $("#editObjForm").serialize(),
        dataType: "json",
        type : "post",
        success : function(data) {
            if (data.status == 0) {
                showUserMessage('Objekt gespeichert!');
            } else {
                showErrMessage(data.result);
            }
        }
    });
}


function SendEditAllForm() {
    var xForm = $("#editObjForm");
    var sURL = "index.php";
    $("#statusAnzeige").html("<strong>Bitte einen Moment noch warten, Artikel werden bearbeitet. Dies kann durchaus ein paar Minuten dauern.</strong>");
    $.ajax({
        url: sURL,
        data : $("#editObjForm").serialize(),
        dataType: "json",
        type : "POST",
        success : function(data) {
            $("#statusAnzeige").html("");
            if (data.status == 0) {
                showUserMessage('Objekte wurden erfolgreich gespeichert!');
            } else {
                showErrMessage(data.result);
            }
        }
    });
}

/*
* Funktion zum Anzeigen der Benutzermeldung
* */
function showUserMessage (sText) {
    var nHideDelay = 2000; // Zeit, wie lange die Meldung angezeigt wird (ms)

    if (jQuery("#gn2_bf_userMsg").hasClass("errMsg")) {
        jQuery("#gn2_bf_userMsg").removeClass("errMsg");
    }

    jQuery("#gn2_bf_userMsg").html(sText);
    jQuery("#gn2_bf_userMsg").show(0).delay(nHideDelay).hide(0);
}

/*
* Funktion zum Anzeigen der Fehlermeldung
* */
function showErrMessage (sText) {
    var nHideDelay = 4000; // Zeit, wie lange die Meldung angezeigt wird (ms)
    jQuery("#gn2_bf_userMsg").addClass("errMsg");
    jQuery("#gn2_bf_userMsg").html(sText);

    /*
     * Message wird eingeblendet und anschl. wieder ausgeblendet,
     * nach dem Ausblenden wird die Klasse errMsg wieder entfernt
     */

    jQuery("#gn2_iv_userMsg").show(0).delay(nHideDelay).hide(0).queue(function(next){
        $(this).removeClass("errMsg");
        next();
    });

}