<?php
namespace gn2\Baerbelfish\Controller;

class PluginEdit extends Base
{
    public function display()
    {
        $strPluginId = $_REQUEST['plugin'];
        $functionActive = $_REQUEST['function'];

        $strLanguageSource = "";
        if (isset($_REQUEST['langSource'])) {
            $strLanguageSource = $_REQUEST['langSource'];
        }
        $strLanguageDest = "";
        if (isset($_REQUEST['langDest'])) {
            $strLanguageDest = $_REQUEST['langDest'];
        }

        $this->_activePlugin->setSourceLanguage($strLanguageSource);
        $this->_activePlugin->setDestLanguage($strLanguageDest);

        $objId = $_REQUEST['objId'];

        $detFunc = $_REQUEST['detFunc'];

        $this->data('pageTitle', $this->_activePlugin->getName());
        $this->data('view', 'edit.html');

        $this->data('strPluginId', $strPluginId);
        $this->data('functionActive', $functionActive);

        $this->data('strLanguageSource', $strLanguageSource);
        $this->data('strLanguageDest', $strLanguageDest);

        $this->data('objId', $objId);

        $bShowHeader = false;
        $this->data('showHeader', $bShowHeader);

        $bShowHtmlHeader = false;
        $this->data('showHtmlHeader', $bShowHtmlHeader);

        if ($_REQUEST['detFunc'] == "saveObj") {
            $this->data('view', 'save.html');

            $NewValues = $_REQUEST['newValues'];
            $this->data('NewValues', $NewValues);

            $aResult =
                $this->_activePlugin->save($objId, $strLanguageDest, $NewValues);

            echo(json_encode($aResult));

            $bShowHeader = false;
            $this->data('showHeader', $bShowHeader);

            $bShowHtmlHeader = false;
            $this->data('showHtmlHeader', $bShowHtmlHeader);
        }

        echo $this->_view->render('index.html', $this->_dataLayer);
    }
}