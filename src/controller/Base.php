<?php
namespace gn2\Baerbelfish\Controller;

abstract class Base
{
    protected $_plugins;

    public function __construct($plugins, $activePlugin)
    {
        $this->_plugins = $plugins;
        $this->_activePlugin = $activePlugin;

        $this->data('plugins', $this->_plugins);
        $this->data('activePlugin', $this->_activePlugin);
    }

    public function data($key, $value)
    {
        $this->_dataLayer[$key] = $value;
    }

    protected $_view;

    abstract public function display();
    public function setView($view)
    {
        $this->_view = $view;
    }
}