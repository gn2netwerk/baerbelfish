<?php
namespace gn2\Baerbelfish\Controller;

class Start extends Base
{
    public function display()
    {
        $bShowHeader = true;
        $bShowHtmlHeader = true;

        echo $this->_view->render('index.html', array(
                'pageTitle' => 'Startseite',
                'showHtmlHeader' => $bShowHtmlHeader,
                'showHeader' => $bShowHeader,
                'plugins'   => $this->_plugins
            )
        );
    }
}