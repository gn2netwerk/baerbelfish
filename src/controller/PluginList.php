<?php
namespace gn2\Baerbelfish\Controller;

class PluginList extends Base
{
    public function display()
    {
        $nStartResult = 0;
        $nLimitPerPage = 50;
        if (isset($_REQUEST['limit'])) {
            $nLimitPerPage = $_REQUEST['limit'];
            $nLimitPerPage = (int)$nLimitPerPage;
        }

        if (isset($_REQUEST['page'])) {
            $nStartResult = $_REQUEST['page'];
            $nStartResult = (int)$nStartResult;
        }

        $strPluginId = $_REQUEST['plugin'];
        $functionActive = $_REQUEST['function'];

        $currFilter = $_REQUEST['filter'];
        $this->_activePlugin->setFilter($currFilter);

        $strLanguageSource = "";
        if (isset($_REQUEST['langSource'])) {
            $strLanguageSource = $_REQUEST['langSource'];
        }
        $strLanguageDest = "";
        if (isset($_REQUEST['langDest'])) {
            $strLanguageDest = $_REQUEST['langDest'];
        }

        $this->_activePlugin->setSourceLanguage($strLanguageSource);
        $this->_activePlugin->setDestLanguage($strLanguageDest);
        $strLanguageDest = $this->_activePlugin->getDestLanguage();

        $this->data('modalTitle', $this->_activePlugin->getListModalTitle());

        $this->data('pageTitle', $this->_activePlugin->getName());
        $this->data('view', 'list.html');
        $this->data('nStartResult', $nStartResult);
        $this->data('nLimitPerPage', $nLimitPerPage);

        $this->data('strPluginId', $strPluginId);
        $this->data('functionActive', $functionActive);

        $this->data('strLanguageSource', $strLanguageSource);
        $this->data('strLanguageDest', $strLanguageDest);

        $bShowHeader = true;
        $this->data('showHeader', $bShowHeader);

        $bShowHtmlHeader = true;
        $this->data('showHtmlHeader', $bShowHtmlHeader);

        $this->data('includePagination', 'includes/pagination.html');
        $this->data('includeModal', 'includes/modal.html');

        $this->data('includeFilter', 'includes/filter.html');
        $this->data('includeSummary', 'includes/summary.html');

        $strShopID = "";
        if (isset($_REQUEST['shopID'])) {
            $strShopID = $_REQUEST['shopID'];
        }

        $sListFilter = "";
        if (count($currFilter) > 0) {
            $arrKeys = array_keys($currFilter);
            for ($f = 0; $f < count($arrKeys); $f++) {
                $sKey = $arrKeys[$f];
                $sVal = $currFilter[$sKey];

                $sListFilter .= "&" . "filter[" . $sKey . "]" .  "=" . $sVal;
            }
        }



        $this->data('strShopID', $strShopID);
        $this->data('currFilter', $sListFilter);



        echo $this->_view->render('index.html', $this->_dataLayer);
    }
}