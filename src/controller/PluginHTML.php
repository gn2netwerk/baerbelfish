<?php
namespace gn2\Baerbelfish\Controller;

class PluginHTML extends Base
{
    public function display()
    {
        $strPluginId = $_REQUEST['plugin'];
        $functionActive = $_REQUEST['function'];

        $strLanguage = "";
        if (isset($_REQUEST['language'])) {
            $strLanguage = $_REQUEST['language'];
        }

        $this->_activePlugin->setSourceLanguage($strLanguage);

        $confDet = "";
        if (isset($_REQUEST['confDet'])) {
            $confDet = $_REQUEST['confDet'];
        }

        $objId = $_REQUEST['objId'];

        $this->data('pageTitle', $this->_activePlugin->getName());
        $this->data('view', 'showhtml.html');

        $this->data('strPluginId', $strPluginId);
        $this->data('functionActive', $functionActive);

        $this->data('strLanguage', $strLanguage);
        $this->data('confDet', $confDet);

        $this->data('objId', $objId);

        $bShowHeader = false;
        $this->data('showHeader', $bShowHeader);

        $bShowHtmlHeader = false;
        $this->data('showHtmlHeader', $bShowHtmlHeader);

        echo $this->_view->render('index.html', $this->_dataLayer);
    }
}