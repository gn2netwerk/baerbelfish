<?php
/**
 * gn2 :: Baerbelfish
 *
 * PHP version 5
 *
 * @category gn2 :: Baerbelfish
 * @package  gn2 :: Baerbelfish
 * @author   Dave Holloway <dh@gn2-netwerk.de>
 * @author   Kristian Berger <kb@gn2-netwerk.de>
 * @license  GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * @version  GIT: <git_id>
 * @link     http://www.gn2-netwerk.de/
 */
namespace gn2\Baerbelfish\Plugin;
use gn2\Baerbelfish\Core\Filter;
use gn2\Baerbelfish\Core\Translation;

include_once dirname(__FILE__).'/classes/Oxid5Plugin.php';

/**
 * Translate_Plugin_Oxid5_Articles
 *
 * PHP version 5
 *
 * @category gn2 :: Bärbelfish
 * @package  gn2 :: Bärbelfish
 * @author   Dave Holloway <dh@gn2-netwerk.de>
 * @author   Kristian Berger <kb@gn2-netwerk.de>
 * @license  GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * * @version  Release: <package_version>
 * @link     http://www.gn2-netwerk.de/
 */
class Oxid5_Articles extends OXID5_Plugin
{
    /**
     * @var array
     */
    public $_currentFilter = array();

    /**
     * @var null
     */
    protected $_shopID = null;
    /**
     * @var null
     */
    protected $_sourceLanguage = null;
    /**
     * @var null
     */
    protected $_destinationLanguage = null;

    public function getNaviIcon() {
        if (file_exists(dirname(__FILE__).'/img/navi_icons/oxid_articles.png') ) {
            return dirname(__FILE__).'/img/navi_icons/oxid_articles.png';
        } else {
            return dirname(__FILE__) . '/img/navi_icons/default.png';
        }
    }

    /**
     * @param null $newShopID
     */
    public function setShopID ($newShopID = null) {
        $this->_shopID = $newShopID;
    }

    /**
     * @return null
     */
    public function getShopID () {
        return $this->_shopID;
    }

    /**
     * @param null $newLang
     */
    public function setSourceLanguage ($newLang = null) {
        $this->_sourceLanguage = $newLang;
    }

    /**
     * @return null
     */
    public function getSourceLanguage () {
        return $this->_sourceLanguage;
    }

    /**
     * @param null $newLang
     */
    public function setDestLanguage ($newLang = null) {
        $sDefLang = $this->getDefaultLanguage();

        if ($newLang === null || $newLang === "") {
            $aAllLang = $this->getLangList();

            foreach($aAllLang as $lang) {
                if ($lang['id'] != $sDefLang) {
                    $newLang = $lang['id'];
                    break;
                }
            }
        }

        if ($sDefLang != $newLang && $this->getSourceLanguage() != $newLang) {
            $this->_destinationLanguage = $newLang;
        } else {
            $this->_destinationLanguage = null;
        }
    }

    /**
     * @return null
     */
    public function getDestLanguage () {
        return $this->_destinationLanguage;
    }


    /**
     * @return array
     */
    protected function _getConfig()
    {
        $arrConfig = array();

        $arrConfig[] = array("Titel", "Pflicht", "text", 255);
        $arrConfig[] = array("Kurzbeschreibung", "Pflicht", "text", 255);
        $arrConfig[] = array("Langbeschreibung", "Optional", "html", -1);
        $arrConfig[] = array("Suchbegriffe", "Optional", "text", 255);
        $arrConfig[] = array("Stichworte", "Optional", "text", 255);

        // $arrConfig[] = array("SEO URL", "Optional", "text", 255);
        $arrConfig[] = array("SEO Keywords", "Optional", "textarea", -1);
        $arrConfig[] = array("SEO Description", "Optional", "textarea", -1);


        return $arrConfig;
    }

    /**
     * return the id of plugin, used in Link-Parameter
     *
     * @return string
    */
    public function getId()
    {
        return 'oxid.articles';
    }

    /**
     * return the name of plugin, used in Link-Text
     *
     * @return string
     */
    public function getName()
    {
        return 'OXID Artikel';
    }

    /**
     * @return string
     */
    public function getListModalTitle() {
        return 'OXID Artikeldetail';
    }

    /**
     * @param array $newFilter
     */
    public function setFilter($newFilter = array()) {
        $this->_currentFilter = $newFilter;
    }

    /**
     * @return array
     */
    public function getFilter() {
        return $this->_currentFilter;
    }

    /**
     * @param bool $bCountSQL
     * @param string $nStartRes
     * @param string $nLimitRes
     * @return string
     */
    protected function _buildSQL($bCountSQL = false, $nStartRes = "", $nLimitRes = "") {
        $currentFilter = $this->getFilter();

        if(empty($_REQUEST['langSource'])){
            $this->setSourceLanguage(1);
        }
        if(empty($_REQUEST['shopID'])){
            $this->setShopID(1);
        }

        switch ($this->getSourceLanguage()) {
            case 1:
                $sLang = "de";
                break;
            case 2:
                $sLang = "en";
                break;
            case
                $sLang = "fr";
                break;
            case
                $sLang = "fl";
                break;
            case
                $sLang = "it";
                break;
            default:
                $sLang = "de";
        }

        $sSelect = "select * ";
//        $sFrom = "from oxarticles ";
        $sFrom = "from oxv_oxarticles_".$this->getShopID()."_".$sLang." ";

        if ($currentFilter["filter_variants"] == "1") {
            $sWhere = "where 1 ";
        } else {
            $sWhere = "where 1 AND OXVARSELECT = '' ";
        }
        $sWhere .= "AND OXSHOPID = ".$this->getShopID()." ";

        if (count($currentFilter) > 0) {
            if ($currentFilter["filter_active"] == "1") {
                $sWhere .= " AND OXACTIVE = 1 ";
            }

            $sManufacturerID = $currentFilter["filter_manufacturer"];
            if ($sManufacturerID == "-1") $sManufacturerID = "";
            if ($sManufacturerID != "") {
                $sWhere .= ' AND OXMANUFACTURERID="' .
                    $sManufacturerID . '"';
            }

            $sCatID = $currentFilter["filter_cat"];
            if ($sCatID == "-1") $sCatID = "";
            if ($sCatID != "") {
                $sFrom .= ', oxobject2category ';
                $sWhere .= ' AND oxobject2category.oxobjectid = oxarticles.oxid ';
                $sWhere .= ' AND oxobject2category.oxcatnid = "'. $sCatID.'" ';
            }

            if ($currentFilter["filter_titlesearch"] != "") {
                $sWhere .= ' AND (OXTitle LIKE "%' .
                    $currentFilter["filter_titlesearch"] . '%"'.
                    'OR OXSHORTDESC LIKE "%' .
                    $currentFilter["filter_titlesearch"] . '%")';
            }

        }

        if ($currentFilter['filter_translationstatus'] > 0) {
            $aSQLStatusWhere = $this->getFilteredResultsByStatus(
                $sSelect, $sFrom, $sWhere, $currentFilter);

            if ($aSQLStatusWhere != null && count($aSQLStatusWhere) > 0) {
                $sOxIdAll = join("', '", $aSQLStatusWhere);
                $sOxIdAll = "'" . $sOxIdAll . "'";
                // echo($sOxIdAll);

                $sWhere .= ' AND OXID IN (' . $sOxIdAll . ') ';
            } else {
                $sWhere .= ' AND 0 ';
            }
        }

        $sSQLBase = $sSelect . $sFrom . $sWhere;
        if ($bCountSQL) {
            return $sSQLBase;
        }

        $sOrder = "order by oxartnum ";
        $nStartLimit = 0;
        if ($nStartRes > 0) {
            $nStartLimit = $nStartRes * $nLimitRes;
        }
        $sLimit = "";
        if ($nLimitRes != "-1") {
            $sLimit = "limit " . $nStartLimit . ", " . $nLimitRes . " ";
        }

        $sSQLBase .= $sOrder . $sLimit;
       // echo($sSQLBase . "<br>");
        return $sSQLBase;
    }

    /**
     * @param string $sSelect
     * @param string $sFrom
     * @param string $sWhere
     * @param null $filter
     * @return array
     */
    public function getFilteredResultsByStatus($sSelect = "", $sFrom = "",
                                               $sWhere = "", $filter = null) {
        if ($sSelect == "" || $sFrom == "" || $sWhere == "") {
            return array();
        }
        $sSQL = $sSelect . $sFrom . $sWhere;
      //  echo("test: " . $sSQL . "<br>");
        $oDb = \oxDb::getDb();
        $rs = $oDb->Execute($sSQL);

        $artLanguage = $this->getSourceLanguage();
        $aReturn = array();

        if ($rs->RecordCount() > 0) {
            while (!$rs->EOF) {
                $sOxId = $rs->fields[0];

                $oArticle = oxNew('oxarticle');
                $oArticle->loadInLang($artLanguage, $sOxId);

                $translationStatus = $this->getTranslationStatus($oArticle);

                $bValid = true;
                switch ($filter['filter_translationstatus']) {
                    case 1: // nicht alles übersetzt
                        if ($translationStatus[0] == $translationStatus[1] &&
                            $translationStatus[2] == $translationStatus[3]) {
                            $bValid = false;
                        }
                        break;
                    case 2: // Pflichtfelder fehlen
                        if ($translationStatus[0] == $translationStatus[1]) {
                            $bValid = false;
                        }
                        break;
                    case 3: // Optionale Felder fehlen
                        if ($translationStatus[2] == $translationStatus[3]) {
                            $bValid = false;
                        }
                        break;
                    case 4: // alles übersetzt
                        if ($translationStatus[0] < $translationStatus[1] ||
                            $translationStatus[2] < $translationStatus[3]) {
                            $bValid = false;
                        }
                        break;
                }

                if ($bValid) {
                    $aReturn[] = $sOxId;
                }

                $rs->moveNext();
            }
        }

        return $aReturn;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        $baseSQL = $this->_buildSQL(true);
        $sSQL = $baseSQL;
        // echo($sSQL);
        // echo("<br><br><br>");
        $oDb = \oxDb::getDb();
        $rs = $oDb->Execute($sSQL);

        return $rs->RecordCount();
    }

    /**
     * @param int $nStartRes
     * @param int $nLimitRes
     * @return array
     */
    public function getRows($nStartRes = 0, $nLimitRes = 50)
    {
        $artLanguage = $this->getSourceLanguage();

        $baseSQL = $this->_buildSQL(false, $nStartRes, $nLimitRes);
        $sSQL = $baseSQL;
        //  echo($sSQL . "<br>");
        $oDb = \oxDb::getDb();
        $rs = $oDb->Execute($sSQL);

        $arrObjects = array();
        if ($rs->RecordCount() > 0) {
            while (!$rs->EOF) {
                $sOxId = $rs->fields[0];

                $oArticle = oxNew('oxarticle');
                $oArticle->loadInLang($artLanguage, $sOxId);
                $arrObjects[] = $oArticle;

                $rs->moveNext();
            }
        }
        return $arrObjects;
    }

    /**
     * @param null $oArticle
     *
     * @return string
     */
    public function getObjectInternalId($oArticle = null)
    {
        if ($oArticle == null) {
            return "";
        }
        return $oArticle->oxarticles__oxid->value;
    }

    /**
     * @param null $oArticle
     *
     * @return string
     */
    public function getObjectId($oArticle = null)
    {
        if ($oArticle == null) {
            return "";
        }
        return $oArticle->oxarticles__oxartnum->value;
    }

    /**
     * @param null $oArticle
     *
     * @return string
     */
    public function getObjectTitle($oArticle = null)
    {
        if ($oArticle == null) {
            return "no Title";
        }
        return $oArticle->oxarticles__oxtitle->value;
    }

    /**
     * @param null $oArticle
     * @return string
     */
    public function getTranslationStatus($oArticle = null)
    {

        $newLanguage = $this->getDestLanguage();
        $oldLanguage = $this->getSourceLanguage();

        $aReturnStatus = array();

        if ($oArticle != null && $newLanguage != null) {
            $sOxidArticle = $this->getObjectInternalId($oArticle);

            $oArticleNew = oxNew('oxarticle');
            $oArticleNew->loadInLang($newLanguage, $sOxidArticle);

            $nCounterMandatory = 0;
            $nCounterMandatoryTarget = 0;
            $nCounterOptional = 0;
            $nCounterOptionalTarget = 0;

            foreach ($this->_getConfig() as $singleConf) {
                $sSrcValue = $this->getContentValue($singleConf[0], $oArticle, $oldLanguage);
                $sCurValue = $this->getContentValue($singleConf[0], $oArticleNew, $newLanguage);

                if ($singleConf[1] == "Pflicht") {
                    if ($sCurValue != "") {
                        $nCounterMandatory++;
                    }
                    $nCounterMandatoryTarget++;
                } else {
                    // check, of the source of optional field is set,
                    // if not then the user don't have to translate
                    if ($sSrcValue != "") {
                        if ($sCurValue != "") {
                            $nCounterOptional++;
                        }
                        $nCounterOptionalTarget++;
                    }

                }
            }

            $aReturnStatus[0] = $nCounterMandatory;
            $aReturnStatus[1] = $nCounterMandatoryTarget;
            $aReturnStatus[2] = $nCounterOptional;
            $aReturnStatus[3] = $nCounterOptionalTarget;
        }
        return $aReturnStatus;
    }

    /**
     * @param $objId
     * @param $confDet
     */
    public function getHTMLValue ($objId, $confDet) {
        $sReturn = "";

        $oArticle = oxNew('oxarticle');
        $oArticle->loadInLang($this->getSourceLanguage(), $objId);

        $arrConfig = $this->_getConfig();
        $sReturn = $this->getContentValue($arrConfig[$confDet][0], $oArticle,
            $this->getSourceLanguage());

        echo($sReturn);
    }

    /**
     * @param $confType
     * @param $oArticle
     * @param $sLang
     * @return string
     */
    public function getContentValue($confType, $oArticle, $sLang)
    {
        switch ($confType) {
        case "Titel" :
            return $this->getObjectTitle($oArticle);
            break;

        case "Kurzbeschreibung" :
            return $oArticle->oxarticles__oxshortdesc->value;
            break;
        case "Langbeschreibung" :
            return $oArticle->getLongDescription();
            break;
        case "Suchbegriffe" :
            return $oArticle->oxarticles__oxsearchkeys->value;
            break;
        case "Stichworte" :
            $oArticleTags = oxNew('oxarticletaglist');
            $oArticleTags->loadInLang($sLang, $oArticle->oxarticles__oxid->value);
            return $oArticleTags->get()->__toString();
            break;

        case "SEO Keywords" :
            $oxSeoEncoder = oxNew("oxSeoEncoderArticle");
            return $oxSeoEncoder->getMetaData( $oArticle->oxarticles__oxid->value, "oxkeywords", null, $sLang);
            break;
        case "SEO Description" :
            $oxSeoEncoder = oxNew("oxSeoEncoderArticle");
            return $oxSeoEncoder->getMetaData( $oArticle->oxarticles__oxid->value, "oxdescription", null, $sLang);
            break;

        }

        return "";
    }

    /**
     * @param $confType
     * @param $oArticle
     * @param $newValue
     * @param $aParamsTemp
     * @return mixed
     */
    public function setContentValue($confType, $oArticle, $newValue, $aParamsTemp, $sLang)
    {
        switch ($confType) {
        case "Titel" :
            $aParamsTemp['oxarticles__oxtitle'] = trim($newValue);
            break;

        case "Kurzbeschreibung" :
            $aParamsTemp['oxarticles__oxshortdesc'] = trim($newValue);
            break;
        case "Langbeschreibung" :
            $aParamsTemp['oxarticles__oxlongdesc'] = trim($newValue);
            break;
        case "Suchbegriffe" :
            $aParamsTemp['oxarticles__oxsearchkeys'] = trim($newValue);
            break;
        case "Stichworte" :
            $oArticleTags = oxNew('oxarticletaglist');
            $oArticleTags->loadInLang($sLang, $oArticle->oxarticles__oxid->value);
            $oArticleTags->set( trim($newValue) );
            $oArticleTags->save();

            break;
        case "SEO Keywords" :
            $strLangDest = $this->getDestLanguage();
            if ($strLangDest == "") $strLangDest = "-1";
            $oArticleSeo = oxnew("oxSeoEncoderArticle");
            $sOxid = $oArticle->oxarticles__oxid->value;

            $this->saveSeoData($oArticleSeo, $sOxid,
                \oxRegistry::getConfig()->getShopId(), $strLangDest, $newValue, "oxkeywords");

            break;
        case "SEO Description" :
            $strLangDest = $this->getDestLanguage();
            if ($strLangDest == "") $strLangDest = "-1";
            $oArticleSeo = oxnew("oxSeoEncoderArticle");
            $sOxid = $oArticle->oxarticles__oxid->value;
            $this->saveSeoData($oArticleSeo, $sOxid,
                \oxRegistry::getConfig()->getShopId(), $strLangDest, $newValue, "oxdescription");

            break;
        }

        return $aParamsTemp;
    }

    function saveSeoData($oSeoObject, $objectid, $shopid, $iLang, $newValue, $sType) {
        try {
            $oDb = \oxDb::getDb();
            $objectid = $oDb->quote($objectid);
            $shopid = $oDb->quote($shopid);


            $oStr = getStr();
            if ( $newValue !== false ) {
                $newValue = $oDb->quote( $oStr->htmlspecialchars(
                    $oSeoObject->encodeString( $oStr->strip_tags( $newValue ),
                        false, $iLang )
                ) );
            }

            $sQ = "insert into oxobject2seodata
                       ( oxobjectid, oxshopid, oxlang, $sType )
                   values
                       ( {$objectid}, {$shopid}, {$iLang}, ".( $newValue ? $newValue  : "''" )." )
                   on duplicate key update
                       $sType = ".( $newValue ? $newValue : "$sType" )." ";

            // echo($sQ);
            $oDb->execute( $sQ );
        } catch (Exception $e) {
            $aReturn['status'] = 1;
            $aReturn['result'] = $e;

            echo(json_encode($aReturn));
            die();

        }
    }



        /**
     * @param $sValue
     * @return mixed
     */
    public function _processLongDesc ($sValue) {
        $sValue = str_replace( '&amp;nbsp;', '&nbsp;', $sValue );
        $sValue = str_replace( '&amp;', '&', $sValue );
        $sValue = str_replace( '&quot;', '"', $sValue );
        $sValue = str_replace( '&lang=', '&amp;lang=', $sValue);
        $sValue = str_replace( '<p>&nbsp;</p>', '', $sValue);
        $sValue = str_replace( '<p>&nbsp; </p>', '', $sValue);
        return $sValue;
    }

    /**
     * @param $searchObjectId
     * @param $sLangDest
     * @param $aValues
     */
    public function save($searchObjectId, $sLangDest, $aValues)
    {
        $oArticleDest = oxNew('oxarticle');
        $oArticleDest->loadInLang($sLangDest, $searchObjectId);

        $aParams = array();

        $aConfig = $this->_getConfig();
        if (count($aConfig) != count($aValues)) {
            $aReturn['status'] = 1;
            // ToDo: Mehrsprachigkeit
            $aReturn['result'] = "Wrong number of fields";
            return $aReturn;
        }

        for ($i = 0; $i < count($aConfig); $i++) {
            $aParams = $this->setContentValue(
                $aConfig[$i][0], $oArticleDest, $aValues[$i], $aParams, $sLangDest
            );
        }
        $oArticleDest->assign($aParams);

        $sTemp = $this->_processLongDesc( $aParams['oxarticles__oxlongdesc'] );
        $oArticleDest->setArticleLongDesc($sTemp);

        try {
        $oArticleDest->save();
        } catch (Exception $e) {
            $aReturn['status'] = 1;
            $aReturn['result'] = $e;

            return $aReturn;

        }

        $aReturn['status'] = 0;
        $aReturn['result'] = "OK";

        return $aReturn;
    }

    /**
     * @param $searchObjectId
     * @param $sLangSrc
     * @param $sLangDest
     */
    public function getCompleteObject($searchObjectId, $sLangSrc, $sLangDest)
    {
        $oArticleSrc = oxNew('oxarticle');
        $oArticleSrc->loadInLang($sLangSrc, $searchObjectId);

        $oArticleDest = oxNew('oxarticle');
        $oArticleDest->loadInLang($sLangDest, $searchObjectId);

        $pluginLanguages = $this->getLangList();

        $sLangSrcName = "";
        $sLangDestName = "";
        foreach ($pluginLanguages as $lang) {
            if ($lang['id'] == $sLangSrc) {
                $sLangSrcName = $lang['name']; break;
            }
        }
        foreach ($pluginLanguages as $lang) {
            if ($lang['id'] == $sLangDest) {
                $sLangDestName = $lang['name']; break;
            }
        }

        $aReturnValues = array();

        $aLanguages = array();
        $singleLang = array();
        $singleLang['id'] = $sLangSrc;
        $singleLang['name'] = $sLangSrcName;
        $aLanguages['source'] = $singleLang;

        $singleLang = array();
        $singleLang['id'] = $sLangDest;
        $singleLang['name'] = $sLangDestName;
        $aLanguages['destination'] = $singleLang;

        $aReturnValues['languages'] = $aLanguages;

        $aValues = array();

        foreach ($this->_getConfig() as $singleConf) {
            $singleValue = array();

            $strContentSrc = $this->getContentValue($singleConf[0], $oArticleSrc, $sLangSrc);
            $strContentDest = $this->getContentValue($singleConf[0], $oArticleDest, $sLangDest);

            // $strContentDest = htmlentities($strContentDest, ENT_QUOTES, "UTF-8");

            $inputType = strtolower($singleConf[2]);
            $inputMaxLength = $singleConf[3];

            $singleValue['confName'] = $singleConf[0];
            $singleValue['confType'] = $inputType;
            $singleValue['confMaxLength'] = $inputMaxLength;
            $singleValue['confContSrc'] = $strContentSrc;
            $singleValue['confContDest'] = $strContentDest;

            $aValues[] = $singleValue;
        }
        $aReturnValues['values'] = $aValues;
        /*
        echo("<pre>");
        print_r($aReturnValues);
        echo("</pre>");
        */
        return $aReturnValues;
    }


    /**
     *
     */
    public function addSpecialHeader()
    {
        $html = '';
        $html .= '<script src="plugins/oxid5/js/oxid5.js"></script>';
        echo($html);
    }

    /**
     * @return array
     */
    public function getFilterView()
    {
        $currentFilter = $this->getFilter();

        $retFilter = array();

        // Checkbox "aktive Produkte"
        $filter = new Filter();
        $filter->setTitle('nur aktive Produkte');
        $filter->setType('checkbox');
        $filter->setName('filter_active');
        $filter->setValue(1);
        $filter->setCheckedValue($currentFilter['filter_active']);

        $retFilter[] = $filter;

        // TextInput "Produktname"
        $filter = new Filter();
        $filter->setTitle('Produktname');
        $filter->setType('inputText');
        $filter->setName('filter_titlesearch');
        $filter->setValue("");
        $filter->setCheckedValue($currentFilter['filter_titlesearch']);

        $retFilter[] = $filter;


        // Selectbox "Marke"
        $aValues = array();
        $oManufacturerList = oxNew( "oxmanufacturerlist" );
        $oManufacturerList->loadManufacturerList();
        foreach ($oManufacturerList as $oManufacturer) {
            $aValues[] = array($oManufacturer->getId(), $oManufacturer->getTitle() );
        }

        $filter = new Filter();
        $filter->setTitle('Marke');
        $filter->setType('select');
        $filter->setName('filter_manufacturer');
        $filter->setValue($aValues);
        $filter->setSelection($currentFilter['filter_manufacturer']);

        $retFilter[] = $filter;

        // Select "Kategorie"
        $aValues = array();
        $oCatTree = oxNew( "oxCategoryList");
        $oCatTree->loadList();
        foreach ($oCatTree as $oCat) {
            $aValues[] = array($oCat->getId(), $oCat->getTitle() );
        }

        $filter = new Filter();
        $filter->setTitle('Kategorie');
        $filter->setType('select');
        $filter->setName('filter_cat');
        $filter->setValue($aValues);
        $filter->setSelection($currentFilter['filter_cat']);

        $retFilter[] = $filter;

        // Checkbox "Varianten anzeigen"
        $filter = new Filter();
        $filter->setTitle('nur Varianten anzeigen');
        $filter->setType('checkbox');
        $filter->setName('filter_variants');
        $filter->setValue(1);
        $filter->setCheckedValue($currentFilter['filter_variants']);

        $retFilter[] = $filter;

        // Select "Produktstatus"
        $aValues = array();
        $aValues[] = array("1", "nicht alles übersetzt");
        $aValues[] = array("2", "fehlende Pflichtfelder");
        $aValues[] = array("3", "fehlende Optionale Felder");
        $aValues[] = array("4", "alles übersetzt");

        $filter = new Filter();
        $filter->setTitle('Übersetzungsstatus');
        $filter->setType('select');
        $filter->setName('filter_translationstatus');
        $filter->setValue($aValues);
        $filter->setSelection($currentFilter['filter_translationstatus']);

        $retFilter[] = $filter;

        return $retFilter;
    }


    /**
     * @return array
     */
    public function getShopList () {
        $sDefShop = \oxRegistry::getConfig()->getShopConfVar("sDefaultShop");

        if($sDefShop == null){
            $sDefShop = 1;
        }

        if ($this->getShopID() == null) {
            $this->setShopID("1");
        }
        $aShopReturn = array();
        $pluginShop = $this->getShopID();

        $sSelect = "select oxid, oxname ";
        $sFrom = "from oxshops ";
        $sWhere = "where 1 ";
        $sWhere .= "and oxactive = 1 ";

        $sSQL = $sSelect . $sFrom . $sWhere;

        $oDb = \oxDb::getDb();
        $rs = $oDb->Execute($sSQL);

        if ($rs->RecordCount() > 0) {
            while (!$rs->EOF) {
                $sOxId = $rs->fields[0];
                $sOXNAME = $rs->fields[1];
                $aShops[] = array("id" => $sOxId, "shopName" => $sOXNAME);

                $rs->MoveNext();
            }
        }

        foreach ($aShops as $shop) {
            $aTemp = array();
            $aTemp['id'] = $shop['id'];
            $aTemp['name'] = $shop['shopName'];
            $bSelected = false;

//            echo "<pre>";
//            echo $shop['id']."->".filter_input(INPUT_GET, "shopID")."<br>";



            if ($shop['id'] == filter_input(INPUT_GET, "shopID") ) {
                $bSelected = true;
            }

            if ($bSelected) {
                $aTemp['selected'] = 1;
            } else {
                $aTemp['selected'] = 0;
            }
            $aShopReturn[] = $aTemp;
        }
        return $aShopReturn;
    }

    /**
     * @return array
     */
    public function getLangList () {
        $sDefLang = \oxRegistry::getConfig()->getShopConfVar("sDefaultLang");

        if ($this->getSourceLanguage() == null) {
            $this->setSourceLanguage($sDefLang);
        }
        $aLangReturn = array();
        $pluginLang = $this->getLanguages();
        foreach ($pluginLang as $lang) {
            $aTemp = array();
            $aTemp['id'] = $lang->getId();
            $aTemp['name'] = $lang->getName();
            $bSelected = false;
            if ($lang->getId() == $this->getSourceLanguage() ) {
                $bSelected = true;
            }

            if ($bSelected) {
                $aTemp['selected'] = 1;
            } else {
                $aTemp['selected'] = 0;
            }
            $aLangReturn[] = $aTemp;
        }
        return $aLangReturn;
    }


    /**
     * @param int $nStart
     * @param int $nLimit
     * @return array
     */
    public function getResults($nStart = 0, $nLimit = 50) {


        $strLangDest = $this->getDestLanguage();
        if ($strLangDest == "") $strLangDest = "-1";

        $pluginRows = $this->getRows($nStart, $nLimit);
        $aObjectInfo = array();

        foreach ($pluginRows as $pluginObject) {
            $aTranslationStatus = $this->getTranslationStatus($pluginObject);

            $translObj = new Translation();
            $translObj->setInternalId( $this->getObjectInternalId($pluginObject) );
            $translObj->setExternalId( $this->getObjectId($pluginObject) );
            $translObj->setObjectTitle( $this->getObjectTitle($pluginObject) );
            $translObj->setTranslationStatus($aTranslationStatus);

            $aObjectInfo[] = $translObj;
        }

        return $aObjectInfo;
    }


    /**
     * @return array
     */
    public function getSummary()
    {
        $strLangDest = $this->getDestLanguage();
        $pluginRows = $this->getRows(0, "-1");
        $aObjectInfo = array();

        foreach ($pluginRows as $pluginObject) {
            $aTranslationStatus = $this->getTranslationStatus($pluginObject);

            $translObj = new Translation();
            $translObj->setInternalId( $this->getObjectInternalId($pluginObject) );
            $translObj->setExternalId( $this->getObjectId($pluginObject) );
            $translObj->setObjectTitle( $this->getObjectTitle($pluginObject) );
            $translObj->setTranslationStatus($aTranslationStatus);

            $aObjectInfo[] = $translObj;
        }


        $nAnzProductsMandatoryDone = 0;
        $nAnzProductsOptionalDone = 0;
        $nAnzProductsCompleteDone = 0;

        $nSumProductsMandatoryDone = 0;
        $nSumProductsMandatoryTarget = 0;
        $nSumProductsOptionalDone = 0;
        $nSumProductsOptionalTarget = 0;

        $nMaxProducts = 0;
        foreach ($aObjectInfo as $aProduct) {
            $aTranslStatObj = $aProduct->getTranslationStatus();

            $nAnzMandatoryDone      = $aTranslStatObj[0];
            $nAnzMandatoryTarget    = $aTranslStatObj[1];
            $nAnzOptionalDone       = $aTranslStatObj[2];
            $nAnzOptionalTarget     = $aTranslStatObj[3];


            $nSumProductsMandatoryDone += $nAnzMandatoryDone;
            $nSumProductsMandatoryTarget += $nAnzMandatoryTarget;
            $nSumProductsOptionalDone += $nAnzOptionalDone;
            $nSumProductsOptionalTarget += $nAnzOptionalTarget;

            $bMandatoryDone = false;
            $bOptionalDone = false;
            if ($nAnzMandatoryDone == $nAnzMandatoryTarget) {
                $nAnzProductsMandatoryDone++;
                $bMandatoryDone = true;
            }

            if ($nAnzOptionalDone == $nAnzOptionalTarget) {
                $nAnzProductsOptionalDone++;
                $bOptionalDone = true;
            }
            if ($bMandatoryDone && $bOptionalDone) {
                $nAnzProductsCompleteDone++;
            }
            $nMaxProducts++;
        }

        $aReturn = array();
        if ($strLangDest != "") {
            $aReturn[] = array('Produkte (Pflichtfelder komplett)',
                $nAnzProductsMandatoryDone, $nMaxProducts);
            $aReturn[] = array('Produkte (Optionalfelder komplett)',
                $nAnzProductsOptionalDone, $nMaxProducts);
            $aReturn[] = array('Produkte komplett fertig',
                $nAnzProductsCompleteDone, $nMaxProducts);
            $aReturn[] = array('Pflichtfelder',
                $nSumProductsMandatoryDone, $nSumProductsMandatoryTarget);
            $aReturn[] = array('optionale Felder',
                $nSumProductsOptionalDone, $nSumProductsOptionalTarget);
        }
        return $aReturn;
    }


}
?>
