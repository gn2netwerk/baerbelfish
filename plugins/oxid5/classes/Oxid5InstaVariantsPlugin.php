<?php
/**
 * gn2 :: Baerbelfish
 *
 * PHP version 5
 *
 * @category gn2 :: Baerbelfish
 * @package  gn2 :: Baerbelfish
 * @author   Dave Holloway <dh@gn2-netwerk.de>
 * @author   Kristian Berger <kb@gn2-netwerk.de>
 * @license  GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * @version  GIT: <git_id>
 * @link     http://www.gn2-netwerk.de/
 */

/**
 * Translate_Plugin_OXID5_InstaVariants_Plugin
 *
 * PHP version 5
 *
 * @category gn2 :: Baerbelfish
 * @package  gn2 :: Baerbelfish
 * @author   Dave Holloway <dh@gn2-netwerk.de>
 * @author   Kristian Berger <kb@gn2-netwerk.de>
 * @license  GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * * @version  Release: <package_version>
 * @link     http://www.gn2-netwerk.de/
 */
namespace gn2\Baerbelfish\Plugin;

abstract class OXID5_InstaVariants_Plugin extends \gn2\Baerbelfish\Core\Plugin
{
    /**
     * constructor
     */
    final public function __construct()
    {
        if ($this->_isOxid() ) {
            include_once dirname(__FILE__) . '/../../../../bootstrap.php';
        }
    }

    /**
     * check, if bootstrap.php exists in root
     *
     * @return boolean
     */
    protected function _isOxid()
    {
        if (file_exists(dirname(__FILE__) . '/../../../../bootstrap.php')) {
            return true;
        }
        return false;
    }

    /**
     * check, if bootstrap.php exists in root
     *
     * @return boolean
     */
    protected function _isInstaVariants()
    {
        if (is_dir(dirname(__FILE__) . '/../../../../modules/gn2_instavariants/')) {
            return true;
        }
        return false;
    }

    /**
     * check, if the plugin is active
     *
     * @return boolean
     */
    public function isActive()
    {
        return $this->_isOxid();
    }



    /**
     * @return array
     */
    public function getLanguages()
    {
        $arrReturn = array();

        $objLang = \oxRegistry::getLang();
        foreach ($objLang->getLanguageArray() as $oLang) {
            $language = new \gn2\Baerbelfish\Core\Language;
            $language->setId($oLang->id);
            $language->setIso($oLang->abbr);
            $language->setName($oLang->name);
            $arrReturn[] = $language;
        }

        return $arrReturn;


    }

    /**
     * @return string
     */
    public function getDefaultLanguage()
    {
        /*
        $objLang = \oxRegistry::getLang();
        return $objLang->getBaseLanguage();
        */

        return \oxRegistry::getConfig()->getShopConfVar("sDefaultLang");
    }
}
