Info zu Übersetzungen bei oxid5
=============

Folgende Daten können übersetzt werden

Artikel
--------

Bezeichnung         Datentyp            Max. Zeichen
Titel               Text                255             Pflichtfeld
Kurzbeschreibung    Text                255             Pflichtfeld
Langbeschreibung    Textbereich (HTML)                  Optional
Suchbegriff         Text                255             Optional
Stichworte          Text                255             Optional

Metatag Keywords    Textbereich                         Optional
Metat. Descr        Textbereich                         Optional


Attribute
--------

Bezeichnung         Datentyp            Max. Zeichen
Name                Text                255             Pflichtfeld

Kategorien
--------

Content
--------

Instavariants
--------

Hersteller
--------

Zahlungsarten
--------

Lieferanten
--------
