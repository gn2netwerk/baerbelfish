<?php
/**
 * gn2 :: Baerbelfish
 *
 * PHP version 5
 *
 * @category gn2 :: Baerbelfish
 * @package  gn2 :: Baerbelfish
 * @author   Dave Holloway <dh@gn2-netwerk.de>
 * @author   Kristian Berger <kb@gn2-netwerk.de>
 * @license  GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * @version  GIT: <git_id>
 * @link     http://www.gn2-netwerk.de/
 */
namespace gn2\Baerbelfish\Plugin;
use gn2\Baerbelfish\Core\Filter;

error_reporting(BAERBELFISH_ERROR_REPORTING);
/**
 * Translate_Plugin_Oxid5_Articles
 *
 * PHP version 5
 *
 * @category gn2 :: Bärbelfish
 * @package  gn2 :: Bärbelfish
 * @author   Dave Holloway <dh@gn2-netwerk.de>
 * @author   Kristian Berger <kb@gn2-netwerk.de>
 * @license  GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * * @version  Release: <package_version>
 * @link     http://www.gn2-netwerk.de/
 */
class Oxid5_InstaVariantsConverter extends OXID5_Plugin
{
    /**
     * @var array
     */
    public $_currentFilter = array();

    /**
     * @var null
     */
    protected $_shopID = null;
    /**
     * @var null
     */
    protected $_sourceLanguage = null;
    /**
     * @var null
     */
    protected $_destinationLanguage = null;

    public function getNaviIcon() {
        if (file_exists(dirname(__FILE__).'/img/navi_icons/oxid_instavariants2.png') ) {
            return dirname(__FILE__).'/img/navi_icons/oxid_instavariants2.png';
        } else {
            return dirname(__FILE__) . '/img/navi_icons/default.png';
        }
    }

    /**
     * @param null $newShopID
     */
    public function setShopID ($newShopID = null) {
        $this->_shopID = $newShopID;
    }

    /**
     * @return null
     */
    public function getShopID () {
        return $this->_shopID;
    }

    /**
     * @param null $newLang
     */
    public function setSourceLanguage ($newLang = null) {
        $this->_sourceLanguage = $newLang;
    }

    /**
     * @return null
     */
    public function getSourceLanguage () {
        return $this->_sourceLanguage;
    }

    /**
     * @param null $newLang
     */
    public function setDestLanguage ($newLang = null) {
        $sDefLang = $this->getDefaultLanguage();

        if ($newLang === null || $newLang === "") {
            $aAllLang = $this->getLangList();

            foreach($aAllLang as $lang) {
                if ($lang['id'] != $sDefLang) {
                    $newLang = $lang['id'];
                    break;
                }
            }
        }

        if ($sDefLang != $newLang && $this->getSourceLanguage() != $newLang) {
            $this->_destinationLanguage = $newLang;
        } else {
            $this->_destinationLanguage = null;
        }
    }

    /**
     * @return null
     */
    public function getDestLanguage () {
        return $this->_destinationLanguage;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return "OXID gn2-Instavar Converter";
    }

    /**
     * @return string
     */
    public function getId()
    {
        return "oxid.gn2instavariantsconverter";
    }

    /**
     * @return array
     */
    public function getShopList () {
        $sDefShop = \oxRegistry::getConfig()->getShopConfVar("sDefaultShop");

        if($sDefShop == null){
            $sDefShop = 1;
        }

        if ($this->getShopID() == null) {
            $this->setShopID("1");
        }
        $aShopReturn = array();
        $pluginShop = $this->getShopID();

        $sSelect = "select oxid, oxname ";
        $sFrom = "from oxshops ";
        $sWhere = "where 1 ";
        $sWhere .= "and oxactive = 1 ";

        $sSQL = $sSelect . $sFrom . $sWhere;

        $oDb = \oxDb::getDb();
        $rs = $oDb->Execute($sSQL);

        if ($rs->RecordCount() > 0) {
            while (!$rs->EOF) {
                $sOxId = $rs->fields[0];
                $sOXNAME = $rs->fields[1];
                $aShops[] = array("id" => $sOxId, "shopName" => $sOXNAME);

                $rs->MoveNext();
            }
        }

        foreach ($aShops as $shop) {
            $aTemp = array();
            $aTemp['id'] = $shop['id'];
            $aTemp['name'] = $shop['shopName'];
            $bSelected = false;

//            echo "<pre>";
//            echo $shop['id']."->".filter_input(INPUT_GET, "shopID")."<br>";



            if ($shop['id'] == filter_input(INPUT_GET, "shopID") ) {
                $bSelected = true;
            }

            if ($bSelected) {
                $aTemp['selected'] = 1;
            } else {
                $aTemp['selected'] = 0;
            }
            $aShopReturn[] = $aTemp;
        }
        return $aShopReturn;
    }


    /**
     * @return array
     */
    public function getFilterView()
    {
        $currentFilter = $this->getFilter();

        $retFilter = array();

        // Checkbox "aktive Produkte"
        $filter = new Filter();
        $filter->setTitle('nur aktive Produkte');
        $filter->setType('checkbox');
        $filter->setName('filter_active');
        $filter->setValue(1);
        $filter->setCheckedValue($currentFilter['filter_active']);

        $retFilter[] = $filter;

        // TextInput "Produktname"
        $filter = new Filter();
        $filter->setTitle('Produktname');
        $filter->setType('inputText');
        $filter->setName('filter_titlesearch');
        $filter->setValue("");
        $filter->setCheckedValue($currentFilter['filter_titlesearch']);

        $retFilter[] = $filter;

        /*
        // Selectbox "Marke"
        $aValues = array();
        $oManufacturerList = oxNew( "oxmanufacturerlist" );
        $oManufacturerList->loadManufacturerList();
        foreach ($oManufacturerList as $oManufacturer) {
            $aValues[] = array($oManufacturer->getId(), $oManufacturer->getTitle() );
        }

        $filter = new Filter();
        $filter->setTitle('Marke');
        $filter->setType('select');
        $filter->setName('filter_manufacturer');
        $filter->setValue($aValues);
        $filter->setSelection($currentFilter['filter_manufacturer']);

        $retFilter[] = $filter;
        */

        /*
        // Select "Kategorie"
        $aValues = array();
        $oCatTree = oxNew( "oxCategoryList");
        $oCatTree->loadList();
        foreach ($oCatTree as $oCat) {
            $aValues[] = array($oCat->getId(), $oCat->getTitle() );
        }

        $filter = new Filter();
        $filter->setTitle('Kategorie');
        $filter->setType('select');
        $filter->setName('filter_cat');
        $filter->setValue($aValues);
        $filter->setSelection($currentFilter['filter_cat']);

        $retFilter[] = $filter;
        */

        // Select "Produktstatus"
        /*
        $aValues = array();
        $aValues[] = array("1", "nicht alles übersetzt");
        $aValues[] = array("2", "fehlende Komponenten");
        $aValues[] = array("3", "fehlende Eigenschaften");
        $aValues[] = array("4", "fehlende Werte");
        $aValues[] = array("5", "alles übersetzt");

        $filter = new Filter();
        $filter->setTitle('Übersetzungsstatus');
        $filter->setType('select');
        $filter->setName('filter_translationstatus');
        $filter->setValue($aValues);
        $filter->setSelection($currentFilter['filter_translationstatus']);

        $retFilter[] = $filter;
        */
        return $retFilter;
    }

    /**
     * @return array
     */
    public function getLangList () {
        $sDefLang = \oxRegistry::getConfig()->getShopConfVar("sDefaultLang");

        if ($this->getSourceLanguage() == null) {
            $this->setSourceLanguage($sDefLang);
        }
        $aLangReturn = array();
        $pluginLang = $this->getLanguages();
        foreach ($pluginLang as $lang) {
            $aTemp = array();
            $aTemp['id'] = $lang->getId();
            $aTemp['name'] = $lang->getName();
            $aTemp['iso'] = $lang->getIso();
            $bSelected = false;
            if ($lang->getId() == $this->getSourceLanguage() ) {
                $bSelected = true;
            }

            if ($bSelected) {
                $aTemp['selected'] = 1;
            } else {
                $aTemp['selected'] = 0;
            }
            $aLangReturn[] = $aTemp;
        }
        return $aLangReturn;
    }

    /**
     * @param array $newFilter
     */
    public function setFilter($newFilter = array()) {
        $this->_currentFilter = $newFilter;
    }

    /**
     * @param bool $bCountSQL
     * @param string $nStartRes
     * @param string $nLimitRes
     * @return string
     */
    protected function _buildSQL($bCountSQL = false, $nStartRes = "", $nLimitRes = "") {
        $currentFilter = $this->getFilter();

        $sSelect = "select * ";
        $sFrom = "from oxarticles ";

        $sWhere = "where 1 AND GN2InstaVariants != '' ";

        $sSQLBase = $sSelect . $sFrom . $sWhere;
        if ($bCountSQL) {
            return $sSQLBase;
        }

        $sOrder = "order by oxartnum ";

        if ($nStartRes > 0) {
            $nStartLimit = $nStartRes * $nLimitRes;
        }
        $sLimit = "";

        $sSQLBase .= $sOrder . $sLimit;
        return $sSQLBase;
    }

    /**
     * @param string $sSelect
     * @param string $sFrom
     * @param string $sWhere
     * @param null $filter
     * @return array
     */
    public function getFilteredResultsByStatus($sSelect = "", $sFrom = "",
                                               $sWhere = "", $filter = null) {
        if ($sSelect == "" || $sFrom == "" || $sWhere == "") {
            return array();
        }
        $sSQL = $sSelect . $sFrom . $sWhere;
        //  echo("test: " . $sSQL . "<br>");
        $oDb = \oxDb::getDb();
        $rs = $oDb->Execute($sSQL);

        $artLanguage = $this->getSourceLanguage();
        $aReturn = array();
        if ($rs->RecordCount() > 0) {
            while (!$rs->EOF) {
                $sOxId = $rs->fields[0];

                $oArticle = oxNew('oxarticle');
                $oArticle->loadInLang($artLanguage, $sOxId);

                $translationStatus = $this->getTranslationStatusDetail($sOxId);
                $bValid = true;
                switch ($filter['filter_translationstatus']) {
                    case 1: // nicht alles übersetzt
                        if ($translationStatus[6] == $translationStatus[7]) {
                            $bValid = false;
                        }
                        break;
                    case 2: // Komponenten fehlen
                        if ($translationStatus[0] == $translationStatus[1]) {
                            $bValid = false;
                        }
                        break;
                    case 3: // Eigenschaften fehlen
                        if ($translationStatus[2] == $translationStatus[3]) {
                            $bValid = false;
                        }
                        break;
                    case 4: // Werte fehlen
                        if ($translationStatus[4] == $translationStatus[5]) {
                            $bValid = false;
                        }
                        break;
                    case 5: // alles übersetzt
                        if ($translationStatus[6] < $translationStatus[7]) {
                            $bValid = false;
                        }
                        break;
                }

                if ($bValid) {
                    $aReturn[] = $sOxId;
                }

                $rs->moveNext();
            }
        }
        return $aReturn;
    }


    /**
     * @param int $nStartRes
     * @param int $nLimitRes
     * @return array
     */
    public function getRows($nStartRes = 0, $nLimitRes = 50)
    {
        $artLanguage = $this->getSourceLanguage();

        $baseSQL = $this->_buildSQL(false, $nStartRes, $nLimitRes);
        $sSQL = $baseSQL;
        //  echo($sSQL . "<br>");
        $oDb = \oxDb::getDb();
        $rs = $oDb->Execute($sSQL);

        $arrObjects = array();
        if ($rs->RecordCount() > 0) {
            while (!$rs->EOF) {
                $sOxId = $rs->fields[0];


                $oArticle = oxNew('oxarticle');
                $oArticle->loadInLang($artLanguage, $sOxId);

                $oArticle = $this->convertWebserviceLabels($sOxId, $artLanguage);

                $arrObjects[] = $oArticle;

                $rs->moveNext();
            }
        }

        echo("Alle InstaVariants Artikel konvertiert.");
        die();
        return $arrObjects;
    }

    function convertWebserviceLabels($sOxid, $curLanguage) {
        if (ob_get_level() == 0) ob_start();
        set_time_limit(30);

       // echo($sOxid . "<br>");
        $oArticleRet = oxNew('oxarticle');
        $oArticleRet->loadInLang($curLanguage, $sOxid);

        echo("<strong>" . $this->getObjectTitle($oArticleRet) . "</strong> wird konvertiert in:<br>");
        $pluginLanguages = $this->getLangList();

        foreach ($pluginLanguages as $lang) {
            $sLangSrcIso = strtoupper($lang['iso']);
            $oArticleRet->loadInLang($sLangSrcIso, $sOxid);
            echo("Sprache: " . $sLangSrcIso . "<br>");

            // Init. des InstaVariants Parser
            $vParser = $oArticleRet->gn2GetInstaVariants();

            if(is_object($vParser)) {
                $response = $vParser->getVariants(1, 0, "", $sLangSrcIso);

                $compIds = array_keys($response['components']);

                $bFoundLeer = false;
                for ($c = 0; $c < count($compIds); $c++) {
                    $component = $vParser->getComponent($compIds[$c]);
                    $properties = $component->getProperties();
                    foreach ($properties as $property) {
                        $values = $property->getValues("", "", $oArticleRet, $property->getDataId());

                        foreach ($values as $value) {
                            $webserviceLabels = $value->getWebserviceLabel($sLangSrcIso);
                            if ($webserviceLabels == " " || $webserviceLabels == "$") {
                                $sNewWebServLbl = "$";
                                $value->setWebserviceLabel($sNewWebServLbl, $sLangSrcIso);
                                $bFoundLeer = true;

                                foreach ($pluginLanguages as $lang2) {
                                    $sLangSrcIso2 = strtoupper($lang2['iso']);
                                    $webserviceLabels2 = $value->getWebserviceLabel($sLangSrcIso2);
                                    if (count($webserviceLabels) > 0 &&
                                        ($webserviceLabels2 == "" || $webserviceLabels2 == " ")
                                    ) {
                                        $value->setWebserviceLabel($sNewWebServLbl, $sLangSrcIso2);
                                    }
                                }

                            }
                        }
                    }
                }
            }

            if ($bFoundLeer) {
                $neonString = $vParser->encode();
                // echo($neonString);

                $oArticleRet->oxarticles__gn2instavariants->rawValue = trim($neonString);
                $oArticleRet->save();
            }
            ob_flush();
            flush();

        }
        $oArticleRet = null;


        $oArticleRet = oxNew('oxarticle');
        $oArticleRet->loadInLang($curLanguage, $sOxid);
        ob_end_flush();
        return $oArticleRet;
    }


    /**
     * @return mixed
     */
    public function getCount()
    {
        $baseSQL = $this->_buildSQL(true);
        $sSQL = $baseSQL;
        // echo($sSQL);
        // echo("<br><br><br>");
        $oDb = \oxDb::getDb();
        $rs = $oDb->Execute($sSQL);

        return $rs->RecordCount();
    }





    /**
     * @param null $oArticle
     *
     * @return string
     */
    public function getObjectInternalId($oArticle = null)
    {
        if ($oArticle == null) {
            return "";
        }
        return $oArticle->oxarticles__oxid->value;
    }

    /**
     * @param null $oArticle
     *
     * @return string
     */
    public function getObjectId($oArticle = null)
    {
        if ($oArticle == null) {
            return "";
        }
        return $oArticle->oxarticles__oxartnum->value;
    }

    /**
     * @param null $oArticle
     *
     * @return string
     */
    public function getObjectTitle($oArticle = null)
    {
        if ($oArticle == null) {
            return "no Title";
        }
        return $oArticle->oxarticles__oxtitle->value;
    }


    /**
     *
     */
    protected function _getConfig()
    {

    }

    /**
     * @return array
     */
    public function getFilter() {
        return $this->_currentFilter;
    }


    /**
     * @return string
     */
    public function getListModalTitle()
    {
        return "OXID InstaVariant Article";
    }

    /**
     *
     */
    public function addSpecialHeader()
    {

    }

    /**
     * @param $searchObjectId
     * @param $sLangSrc
     * @param $sLangDest
     * @return array
     */
    public function getCompleteObject($searchObjectId, $sLangSrc, $sLangDest)
    {
        $oArticle = oxNew('oxarticle');
        $oArticle->load($searchObjectId);

        $pluginLanguages = $this->getLangList();

        $sLangSrcName = "";
        $sLangDestName = "";


        // ISO Code der Sprache wird für InstaVar Parser benötigt
        $sLangSrcIso = "";
        $sLangDestIso = "";

        foreach ($pluginLanguages as $lang) {
            if ($lang['id'] == $sLangSrc) {
                $sLangSrcName = $lang['name'];
                $sLangSrcIso = strtoupper($lang['iso']);
                break;
            }
        }
        foreach ($pluginLanguages as $lang) {
            if ($lang['id'] == $sLangDest) {
                $sLangDestName = $lang['name'];
                $sLangDestIso = strtoupper($lang['iso']);
                break;
            }
        }

        $aReturnValues = array();

        $aLanguages = array();
        $singleLang = array();
        $singleLang['id'] = $sLangSrc;
        $singleLang['name'] = $sLangSrcName;
        $aLanguages['source'] = $singleLang;

        $singleLang = array();
        $singleLang['id'] = $sLangDest;
        $singleLang['name'] = $sLangDestName;
        $aLanguages['destination'] = $singleLang;

        $aReturnValues['languages'] = $aLanguages;

        $aValues = array();

        // Init. des InstaVariants Parser
        $vParser = $oArticle->gn2GetInstaVariants();

        $aConfigSrc = $this->getInstaData($oArticle, $vParser, $sLangSrcIso);
        $aConfigDest = $this->getInstaData($oArticle, $vParser, $sLangDestIso);

        /*
        echo("<pre>");
        print_r($aConfigSrc);
        print_r($aConfigDest);
        echo("</pre>");
        */

        /*
         * Aufbau der Werte für Formular, Reihenfolge entspricht dem Auftreten
         * im InstaVar. Objekt
         * */
        for ($j = 0; $j < count($aConfigSrc); $j++) {
            $compDataSrc = $aConfigSrc[$j];
            $compDataDest = $aConfigDest[$j];

            $sCompLabelSrc = $compDataSrc['label'];
            $sCompLabelDest = $compDataDest['label'];

            $sCompInfoSrc = $compDataSrc['info'];
            $sCompInfoDest = $compDataDest['info'];

            $singleValue = array();
            $singleValue['confName'] = "Komponente";
            $singleValue['confCSSClass'] = "gn2_bf_iv_component";
            $singleValue['confType'] = "text";
            $singleValue['confMaxLength'] = "";
            $singleValue['confContSrc'] = $sCompLabelSrc;
            $singleValue['confContDest'] = $sCompLabelDest;
            $aValues[] = $singleValue;

            if (strpos($sCompInfoSrc, "CMS-ID") === 0) {
            } else {
                $singleValue = array();
                $singleValue['confName'] = "Komponente Infotext";
                $singleValue['confCSSClass'] = "gn2_bf_iv_comp_info";
                $singleValue['confType'] = "text";
                $singleValue['confMaxLength'] = "";
                $singleValue['confContSrc'] = $sCompInfoSrc;
                $singleValue['confContDest'] = $sCompInfoDest;
                $aValues[] = $singleValue;
            }

            $aPropertiesSrc = $compDataSrc['properties'];
            $aPropertiesDest = $compDataDest['properties'];

            for ($p = 0; $p < count($aPropertiesSrc); $p++) {
                $sPropLabelSrc = $aPropertiesSrc[$p]['label'];
                $sPropLabelDest = $aPropertiesDest[$p]['label'];

                $sPropInfoSrc = $aPropertiesSrc[$p]['info'];
                $sPropInfoDest = $aPropertiesDest[$p]['info'];

                $singleValue = array();
                $singleValue['confName'] = "Eigenschaft";
                $singleValue['confCSSClass'] = "gn2_bf_iv_properties";
                $singleValue['confType'] = "text";
                $singleValue['confMaxLength'] = "";
                $singleValue['confContSrc'] = $sPropLabelSrc;
                $singleValue['confContDest'] = $sPropLabelDest;
                $aValues[] = $singleValue;

                if (strpos($sPropInfoSrc, "CMS-ID") === 0) {
                } else {
                    $singleValue = array();
                    $singleValue['confName'] = "Eigenschaft Infotext";
                    $singleValue['confCSSClass'] = "gn2_bf_iv_prop_info";
                    $singleValue['confType'] = "text";
                    $singleValue['confMaxLength'] = "";
                    $singleValue['confContSrc'] = $sPropInfoSrc;
                    $singleValue['confContDest'] = $sPropInfoDest;
                    $aValues[] = $singleValue;

                }


                $aValuesSrc = $aPropertiesSrc[$p]['values'];
                $aValuesDest = $aPropertiesDest[$p]['values'];

                for ($v = 0; $v < count($aValuesSrc); $v++) {
                    $sValueSrc = $aValuesSrc[$v];
                    $sValueDest = $aValuesDest[$v];

                    // echo('----' . $sValueSrc . " => " . $sValueDest . "<br>");
                    $singleValue = array();
                    $singleValue['confName'] = "Wert";
                    $singleValue['confCSSClass'] = "gn2_bf_iv_values";
                    $singleValue['confType'] = "text";
                    $singleValue['confMaxLength'] = "";
                    $singleValue['confContSrc'] = $sValueSrc;
                    $singleValue['confContDest'] = $sValueDest;
                    $aValues[] = $singleValue;
                }
            }

        }

        $aReturnValues['values'] = $aValues;
        return $aReturnValues;
    }


    /**
     * @param $data
     * @return array
     */
    public function getInstaData($oArticle, $vParser, $sLangIso) {
        $aReturn = array();

        // ToDo: Kontrolle, ob man die Geschwindigkeit optimieren kann
        $response = $vParser->getVariants(1, 0, "", $sLangIso);

        $compIds = array_keys($response['components']);


        for ($c = 0; $c < count($compIds); $c++) {
            $component = $vParser->getComponent($compIds[$c]);

            $sCompLabel = $component->getLabel($sLangIso);
            if ($sCompLabel == " ") $sCompLabel = "$";
            $sCompInfoText = $component->getInfoText($sLangIso);

            $aProperties = array();
            $properties = $component->getProperties();
            foreach ($properties as $property) {
                $sPropLabel = $property->getLabel($sLangIso);
                if ($sPropLabel == " ") $sPropLabel = "$";
                $sPropInfoText = $property->getInfoText($sLangIso);

                $aValues = array();
                $values = $property->getValues("", "",
                    $oArticle, $property->getDataId());

                foreach ($values as $value) {
                    $sValueLabel = $value->getLabel($sLangIso);
                    if ($sValueLabel == " ") $sValueLabel = "$";

                    $webserviveLabels = $value->getAllWebserviceLabels();
                    $langWebserviceLabel = "";
                    if ( count($webserviveLabels) > 0) {
                        $langWebserviceLabel = $webserviveLabels[$sLangIso];
                    }

                    $aValues[] = array($sValueLabel, $langWebserviceLabel);
                }

                $aTempProp = array();
                $aTempProp['label'] = $sPropLabel;
                $aTempProp['info'] = $sPropInfoText;
                $aTempProp['values'] = $aValues;
                $aProperties[] = $aTempProp;
            }

            $aTempComp = array();
            $aTempComp['label'] = $sCompLabel;
            $aTempComp['info'] = $sCompInfoText;
            $aTempComp['properties'] = $aProperties;
            $aReturn[] = $aTempComp;
        }

        return $aReturn;
    }

    /**
     * @param null $searchObjectId
     * @return array
     */
    public function getTranslationStatus($searchObjectId = null)
    {
        /*
         * get translation status of this objects
         * */
        $aReturnStatus = $this->getTranslationStatusDetail($searchObjectId);

        /*
        $newLanguage = $this->getDestLanguage();
        $oldLanguage = $this->getSourceLanguage();

        $aValues = $this->getCompleteObject($searchObjectId, $oldLanguage, $newLanguage);

        $nCounterMandatory = 0;
        $nCounterMandatoryTarget = 0;
        $nCounterOptional = 0;
        $nCounterOptionalTarget = 0;

        $aVal = $aValues['values'];

        $aReturnStatus = array();

        for ($j = 0; $j < count($aVal); $j++) {
            if ($aVal[$j]['confName'] == "Komponente" ||
                $aVal[$j]['confName'] == "Eigenschaft" ||
                $aVal[$j]['confName'] == "Wert") {

                if ( $aVal[$j]['confContDest'] != "") {
                    $nCounterMandatory++;
                }
                $nCounterMandatoryTarget++;
            } else {
                if ($aVal[$j]['confContSrc'] != "") {
                    if ($aVal[$j]['confContDest'] != "") {
                        $nCounterOptional++;
                    }
                    $nCounterOptionalTarget++;
                }

            }

            $aReturnStatus[0] = $nCounterMandatory;
            $aReturnStatus[1] = $nCounterMandatoryTarget;
            $aReturnStatus[2] = $nCounterOptional;
            $aReturnStatus[3] = $nCounterOptionalTarget;
        }
        */

        return $aReturnStatus;
    }

    /**
     * @param null $searchObjectId
     * @return array
     */
    public function getTranslationStatusDetail($searchObjectId = null)
    {
        $newLanguage = $this->getDestLanguage();
        $oldLanguage = $this->getSourceLanguage();

        $aValues = $this->getCompleteObject($searchObjectId, $oldLanguage, $newLanguage);

        $nCounterComponent = 0;
        $nCounterComponentTarget = 0;
        $nCounterProperties = 0;
        $nCounterPropertiesTarget = 0;
        $nCounterValues = 0;
        $nCounterValuesTarget = 0;

        $nCounterGeneral = 0;
        $nCounterGeneralTarget = 0;

        $nCounterOptional = 0;
        $nCounterOptionalTarget = 0;

        $aVal = $aValues['values'];

        $aReturnStatus = array();

        for ($j = 0; $j < count($aVal); $j++) {
            // echo($aVal[$j]['confName'] . "<br>");
            switch ($aVal[$j]['confName']) {
                case "Komponente" :
                    if ( $aVal[$j]['confContDest'] != "") {
                        $nCounterComponent++;
                    }
                    $nCounterComponentTarget++;
                    break;
                case "Eigenschaft" :
                    if ( $aVal[$j]['confContDest'] != "") {
                        $nCounterProperties++;
                    }
                    $nCounterPropertiesTarget++;
                    break;
                case "Wert" :
                    if ( $aVal[$j]['confContDest'] != "") {
                        $nCounterValues++;
                    }
                    $nCounterValuesTarget++;
                    break;
                case "Komponente Infotext" :
                case "Eigenschaft Infotext" :
                    if ( $aVal[$j]['confContSrc'] != "") {
                        if ( $aVal[$j]['confContDest'] != "") {
                            $nCounterOptional++;
                        }
                        $nCounterOptionalTarget++;
                    }
                    break;

            }
            if ($aVal[$j]['confContSrc'] != "") {
                if ($aVal[$j]['confContDest'] != "") {
                    $nCounterGeneral++;
                }
                $nCounterGeneralTarget++;
            }

            $aReturnStatus[0] = $nCounterComponent;
            $aReturnStatus[1] = $nCounterComponentTarget;
            $aReturnStatus[2] = $nCounterProperties;
            $aReturnStatus[3] = $nCounterPropertiesTarget;
            $aReturnStatus[4] = $nCounterValues;
            $aReturnStatus[5] = $nCounterValuesTarget;
            $aReturnStatus[6] = $nCounterGeneral;
            $aReturnStatus[7] = $nCounterGeneralTarget;
            $aReturnStatus[8] = $nCounterOptional;
            $aReturnStatus[9] = $nCounterOptionalTarget;
        }


        return $aReturnStatus;
    }

    /**
     * @return array
     */
    public function getSummary()
    {
        /*
        $strLangDest = $this->getDestLanguage();
        $pluginRows = $this->getRows(0, "-1");
        $aObjectInfo = array();

        foreach ($pluginRows as $pluginObject) {
            $aTranslationStatus = $this->getTranslationStatusDetail($this->getObjectInternalId($pluginObject));

            $translObj = new Translation();
            $translObj->setInternalId( $this->getObjectInternalId($pluginObject) );
            $translObj->setExternalId( $this->getObjectId($pluginObject) );
            $translObj->setObjectTitle( $this->getObjectTitle($pluginObject) );
            $translObj->setTranslationStatus($aTranslationStatus);

            $aObjectInfo[] = $translObj;
        }

        $nAnzProductsComponentDone = 0;
        $nAnzProductsPropertiesDone = 0;
        $nAnzProductsValuesDone = 0;
        $nAnzProductsOptionalDone = 0;

        $nAnzProductsCompleteDone = 0;

        $nSumProductsComponentDone = 0;
        $nSumProductsPropertiesDone = 0;
        $nSumProductsValuesDone = 0;
        $nSumProductsOptionalDone = 0;

        $nSumProductsComponentTarget = 0;
        $nSumProductsPropertiesTarget = 0;
        $nSumProductsValuesTarget = 0;
        $nSumProductsOptionalTarget = 0;

        $nMaxProducts = 0;
        foreach ($aObjectInfo as $aProduct) {
            $aTranslStatObj = $aProduct->getTranslationStatus();

            $nAnzComponentDone = $aTranslStatObj[0];
            $nAnzPropertiesDone = $aTranslStatObj[2];
            $nAnzValuesDone = $aTranslStatObj[4];
            $nAnzOptionalDone = $aTranslStatObj[8];

            $nAnzComponentTarget = $aTranslStatObj[1];
            $nAnzPropertiesTarget = $aTranslStatObj[3];
            $nAnzValuesTarget = $aTranslStatObj[5];
            $nAnzOptionalTarget = $aTranslStatObj[9];


            $nSumProductsComponentDone += $nAnzComponentDone;
            $nSumProductsComponentTarget += $nAnzComponentTarget;
            $nSumProductsPropertiesDone += $nAnzPropertiesDone;
            $nSumProductsPropertiesTarget += $nAnzPropertiesTarget;
            $nSumProductsValuesDone += $nAnzValuesDone;
            $nSumProductsValuesTarget += $nAnzValuesTarget;
            $nSumProductsOptionalDone += $nAnzOptionalDone;
            $nSumProductsOptionalTarget += $nAnzOptionalTarget;

            $bComponentDone = false;
            $bPropertyDone = false;
            $bValueDone = false;
            $bOptionalDone = false;

            if ($nAnzComponentDone == $nAnzComponentTarget) {
                $nAnzProductsComponentDone++;
                $bComponentDone = true;
            }

            if ($nAnzPropertiesDone == $nAnzPropertiesTarget) {
                $nAnzProductsPropertiesDone++;
                $bPropertyDone = true;
            }

            if ($nAnzValuesDone == $nAnzValuesTarget) {
                $nAnzProductsValuesDone++;
                $bValueDone = true;
            }

            if ($nAnzOptionalDone == $nAnzOptionalTarget) {
                $nAnzProductsOptionalDone++;
                $bOptionalDone = true;
            }
            // echo($bComponentDone . " && " . $bPropertyDone . " && " . $bValueDone . " && " . $bOptionalDone . "<br>");

            if ($bComponentDone && $bPropertyDone && $bValueDone && $bOptionalDone) {
                $nAnzProductsCompleteDone++;
            }
            $nMaxProducts++;
        }

        $aReturn = array();
        if ($strLangDest != "") {
            $aReturn[] = array('Produkte (Komponenten übersetzt)',
                $nAnzProductsComponentDone, $nMaxProducts);
            $aReturn[] = array('Produkte (Eigenschaften übersetzt)',
                $nAnzProductsPropertiesDone, $nMaxProducts);
            $aReturn[] = array('Produkte (Werte übersetzt)',
                $nAnzProductsValuesDone, $nMaxProducts);
            $aReturn[] = array('Produkte komplett fertig',
                $nAnzProductsCompleteDone, $nMaxProducts);

            $aReturn[] = array('übersetzte Komponenten',
                $nSumProductsComponentDone, $nSumProductsComponentTarget);
            $aReturn[] = array('übersetzte Eigenschaften',
                $nSumProductsPropertiesDone, $nSumProductsPropertiesTarget);
            $aReturn[] = array('übersetzte Werte',
                $nSumProductsValuesDone, $nSumProductsValuesTarget);
            $aReturn[] = array('übersetzte Infotexte',
                $nSumProductsOptionalDone, $nSumProductsOptionalTarget);
        }
        */
        return $aReturn;
    }

    /**
     * @param $searchObjectId
     * @param $sLangDest
     * @param $aNewValues
     * @return array
     */
    public function save($searchObjectId, $sLangDest, $aNewValues)
    {
        try {
            $oArticle = oxNew('oxarticle');
            $oArticle->load($searchObjectId);

            /*
            echo("<pre>");
            print_r($aNewValues);
            echo("</pre>");
            */

            $pluginLanguages = $this->getLangList();
            $sLangSrc = $this->getSourceLanguage();

            $sLangSrcIso = "";
            $sLangDestIso = "";

            foreach ($pluginLanguages as $lang) {
                if ($lang['id'] == $sLangSrc) {
                    $sLangSrcIso = strtoupper($lang['iso']);
                    break;
                }
            }
            foreach ($pluginLanguages as $lang) {
                if ($lang['id'] == $sLangDest) {
                    $sLangDestIso = strtoupper($lang['iso']);
                    break;
                }
            }

            $vParser = $oArticle->gn2GetInstaVariants();
            $response = $vParser->getVariants(1, 0, "", $sLangSrcIso);

            $nCounterNewValues = 0;
            $compIds = array_keys($response['components']);

            /*
             * das aktuelle Parser Objekt wird durchlaufen, die Reihenfolge
             * der übergebenen Werte entspricht der Reihenfolge innerhalb
             * des Parser Objekts
             * */
            for ($c = 0; $c < count($compIds); $c++) {
                $component = $vParser->getComponent($compIds[$c]);
                // echo("Komponente: (" . $compIds[$c] . "):<br>");
                $sNewValue = $aNewValues[$nCounterNewValues];
                // echo("Neuer Wert: " . $sNewValue . "<br>");
                $component->setLabel($sNewValue, $sLangDestIso);
                $nCounterNewValues++;

                if (strpos($component->getInfoText($sLangSrcIso), "CMS-ID") === 0) {
                } else {
                    $sNewValue = $aNewValues[$nCounterNewValues];
                    // echo("Neuer Wert: " . $sNewValue . "<br>");
                    $component->setInfoText($sNewValue, $sLangDestIso);
                    $nCounterNewValues++;
                }


                $properties = $component->getProperties();
                foreach ($properties as $property) {
                    // echo("Eigenschaft: (" . $property->getDataId() . "):<br>");
                    $sNewValue = $aNewValues[$nCounterNewValues];
                    // echo("Neuer Wert: " . $sNewValue . "<br>");
                    $property->setLabel($sNewValue, $sLangDestIso);
                    $nCounterNewValues++;

                    if (strpos($property->getInfoText($sLangSrcIso), "CMS-ID") === 0) {
                    } else {
                        $sNewValue = $aNewValues[$nCounterNewValues];
                        // echo("Neuer Wert: " . $sNewValue . "<br>");
                        $property->setInfoText($sNewValue, $sLangDestIso);
                        $nCounterNewValues++;
                    }


                    $values = $property->getValues("", "",
                        $oArticle, $property->getDataId());

                    foreach ($values as $value) {
                        // echo("Eigenschaft: (" . $value->getObjectId() . "):<br>");
                        $sNewValue = $aNewValues[$nCounterNewValues];
                        // echo("Neuer Wert: " . $sNewValue . "<br>");
                        $value->setLabel($sNewValue, $sLangDestIso);
                        $nCounterNewValues++;

                    }
                }
            }

            /*
            echo("<pre>");
            print_r($vParser);
            echo("</pre>");
            */

            $neonString = $vParser->encode();
            // echo($neonString);

            $oArticle->oxarticles__gn2instavariants->rawValue = trim($neonString);
            $oArticle->save();
        } catch (Exception $e) {
            $aReturn['status'] = 1;
            $aReturn['result'] = $e;

            return $aReturn;
        }

        $aReturn = array();
        $aReturn['status'] = 0;
        $aReturn['result'] = "OK";

        return $aReturn;
    }

}

