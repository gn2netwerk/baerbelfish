$(document).ready(function (e) {
    $('input[name="filter_active"]').click(function(e) {
        refreshResults();
    });

    $('select[name="filter_manufacturer"]').change(function(e) {
        refreshResults();
    });

    $('select[name="filter_cat"]').change(function(e) {
        refreshResults();
    })
});

function refreshResults() {
    var sValPlugin = jQuery("#plugin").val();
    var sNewLanguageSource = jQuery("#languageSource").val();
    var sNewLanguageDest = jQuery("#languageDestination").val();

    var nFilterCount = 0;
    var bChecked = $('input[name="filter_active"]').is(':checked');
    nFilterCount++;
    var nManufacturerSelected = $('select[name="filter_manufacturer"]').val();
    nFilterCount++;
    var nCatSelected = $('select[name="filter_cat"]').val();
    nFilterCount++;


    var sURL = "?&plugin=" + sValPlugin
        + "&langSource=" + sNewLanguageSource
        + "&langDest=" + sNewLanguageDest
        + "&curFunc=showResults"
        + "&filter_0=" + bChecked
        + "&filter_1=" + nManufacturerSelected
        + "&filter_2=" + nCatSelected
        + "&filter_count=" + nFilterCount;

    $.ajax({
        url : sURL,
        success : function (data) {
            $("#results").html(data);
            initResultButtons();
        }
    });
}