<?php
/**
 * gn2 :: Baerbelfish
 *
 * PHP version 5
 *
 * @category gn2 :: Baerbelfish
 * @package  gn2 :: Baerbelfish
 * @author   Dave Holloway <dh@gn2-netwerk.de>
 * @author   Kristian Berger <kb@gn2-netwerk.de>
 * @license  GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * @version  GIT: <git_id>
 * @link     http://www.gn2-netwerk.de/
 */
namespace gn2\Baerbelfish\Plugin;
use gn2\Baerbelfish\Core\Filter;
use gn2\Baerbelfish\Core\Translation;

include_once dirname(__FILE__).'/classes/Oxid5Plugin.php';

/**
 * Translate_Plugin_Oxid5_CMS
 *
 * PHP version 5
 *
 * @category gn2 :: Bärbelfish
 * @package  gn2 :: Bärbelfish
 * @author   Dave Holloway <dh@gn2-netwerk.de>
 * @author   Kristian Berger <kb@gn2-netwerk.de>
 * @license  GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * * @version  Release: <package_version>
 * @link     http://www.gn2-netwerk.de/
 */
class Oxid5_CMS extends OXID5_Plugin
{
    /**
     * @var array
     */
    public $_currentFilter = array();

    /**
     * @var null
     */
    protected $_shopID = null;
    /**
     * @var null
     */
    protected $_sourceLanguage = null;
    /**
     * @var null
     */
    protected $_destinationLanguage = null;


    public function getNaviIcon() {
        if (file_exists(dirname(__FILE__).'/img/navi_icons/oxid_cms.png') ) {
            return dirname(__FILE__).'/img/navi_icons/oxid_cms.png';
        } else {
            return dirname(__FILE__) . '/img/navi_icons/default.png';
        }
    }

    /**
     * @param null $newShopID
     */
    public function setShopID ($newShopID = null) {
        $this->_shopID = $newShopID;
    }

    /**
     * @return null
     */
    public function getShopID () {
        return $this->_shopID;
    }

    /**
     * @param null $newLang
     */
    public function setSourceLanguage ($newLang = null) {
        $this->_sourceLanguage = $newLang;
    }

    /**
     * @return null
     */
    public function getSourceLanguage () {
        return $this->_sourceLanguage;
    }

    /**
     * @param null $newLang
     */
    public function setDestLanguage ($newLang = null) {
        $sDefLang = $this->getDefaultLanguage();

        if ($newLang === null || $newLang === "") {
            $aAllLang = $this->getLangList();

            foreach($aAllLang as $lang) {
                if ($lang['id'] != $sDefLang) {
                    $newLang = $lang['id'];
                    break;
                }
            }
        }

        if ($sDefLang != $newLang && $this->getSourceLanguage() != $newLang) {
            $this->_destinationLanguage = $newLang;
        } else {
            $this->_destinationLanguage = null;
        }
    }

    /**
     * @return null
     */
    public function getDestLanguage () {
        return $this->_destinationLanguage;
    }


    /**
     * @return array
     */
    protected function _getConfig()
    {
        $arrConfig = array();

        $arrConfig[] = array("Titel", "Pflicht", "text", 255);
        $arrConfig[] = array("Langbeschreibung", "Pflicht", "html", -1);
        $arrConfig[] = array("SEO Keywords", "Optional", "textarea", -1);
        $arrConfig[] = array("SEO Description", "Optional", "textarea", -1);

        return $arrConfig;
    }

    /**
     * return the id of plugin, used in Link-Parameter
     *
     * @return string
    */
    public function getId()
    {
        return 'oxid.cms';
    }

    /**
     * return the name of plugin, used in Link-Text
     *
     * @return string
     */
    public function getName()
    {
        return 'OXID CMS';
    }

    /**
     * @return string
     */
    public function getListModalTitle() {
        return 'OXID CMS Detail';
    }

    /**
     * @param array $newFilter
     */
    public function setFilter($newFilter = array()) {
        $this->_currentFilter = $newFilter;
    }

    /**
     * @return array
     */
    public function getFilter() {
        return $this->_currentFilter;
    }

    /**
     * @param bool $bCountSQL
     * @param string $nStartRes
     * @param string $nLimitRes
     * @return string
     */
    protected function _buildSQL($bCountSQL = false, $nStartRes = "", $nLimitRes = "") {
        $currentFilter = $this->getFilter();

        if(empty($_REQUEST['langSource'])){
            $this->setSourceLanguage(1);
        }
        if(empty($_REQUEST['shopID'])){
            $this->setShopID(1);
        } else {
            $this->setShopID($_REQUEST['shopID']);
        }

        switch ($this->getSourceLanguage()) {
            case 1:
                $sLang = "de";
                break;
            case 2:
                $sLang = "en";
                break;
            case
            $sLang = "fr";
                break;
            case
            $sLang = "fl";
                break;
            case
            $sLang = "it";
                break;
            default:
                $sLang = "de";
        }

        $sSelect = "select * ";
        $sFrom = "from oxv_oxcontents_".$sLang." ";
        $sWhere = "WHERE 1 ";

        $sWhere .= "AND OXSHOPID = ".$this->getShopID()." ";

        if (count($currentFilter) > 0) {
            if ($currentFilter["filter_active"] == "1") {
                $sWhere .= " AND OXACTIVE = 1 ";
            }

            if ($currentFilter["filter_titlesearch"] != "") {
                $sWhere .= ' AND (OXTitle LIKE "%' .
                    $currentFilter["filter_titlesearch"] . '%"'.
                    'OR OXContent LIKE "%' .
                    $currentFilter["filter_titlesearch"] . '%")';
            }

            if ($currentFilter["filter_folder"] != "") {
                if ($currentFilter["filter_folder"] != "CMSFOLDER_NONE") {
                    $sWhere .= " AND OXFOLDER = '" . $currentFilter["filter_folder"] ."' ";
                } else {
                    $sWhere .= " AND OXFOLDER = '' ";
                }
            }
        }

        if ($currentFilter['filter_translationstatus'] > 0) {
            $aSQLStatusWhere = $this->getFilteredResultsByStatus(
                $sSelect, $sFrom, $sWhere, $currentFilter);

            if ($aSQLStatusWhere != null && count($aSQLStatusWhere) > 0) {
                $sOxIdAll = join("', '", $aSQLStatusWhere);
                $sOxIdAll = "'" . $sOxIdAll . "'";
                // echo($sOxIdAll);

                $sWhere .= ' AND OXID IN (' . $sOxIdAll . ') ';
            } else {
                $sWhere .= ' AND 0 ';
            }
        }

        $sSQLBase = $sSelect . $sFrom . $sWhere;
        // echo($sSQLBase . "<br>");
        if ($bCountSQL) {
            return $sSQLBase;
        }

        $sOrder = "order by oxTitle ";
        $nStartLimit = 0;
        if ($nStartRes > 0) {
            $nStartLimit = $nStartRes * $nLimitRes;
        }
        $sLimit = "";
        if ($nLimitRes != "-1") {
            $sLimit = "limit " . $nStartLimit . ", " . $nLimitRes . " ";
        }

        $sSQLBase .= $sOrder . $sLimit;
       // echo($sSQLBase . "<br>");
        return $sSQLBase;
    }

    /**
     * @param string $sSelect
     * @param string $sFrom
     * @param string $sWhere
     * @param null $filter
     * @return array
     */
    public function getFilteredResultsByStatus($sSelect = "", $sFrom = "",
                                               $sWhere = "", $filter = null) {
        if ($sSelect == "" || $sFrom == "" || $sWhere == "") {
            return array();
        }
        $sSQL = $sSelect . $sFrom . $sWhere;
      //  echo("test: " . $sSQL . "<br>");
        $oDb = \oxDb::getDb();
        $rs = $oDb->Execute($sSQL);

        $artLanguage = $this->getSourceLanguage();
        $aReturn = array();

        if ($rs->RecordCount() > 0) {
            while (!$rs->EOF) {
                $sOxId = $rs->fields[0];

                $translationStatus = $this->getTranslationStatus($sOxId);

                $bValid = true;
                switch ($filter['filter_translationstatus']) {
                    case 1: // nicht alles übersetzt
                        if ($translationStatus[0] == $translationStatus[1] &&
                            $translationStatus[2] == $translationStatus[3]) {
                            $bValid = false;
                        }
                        break;
                    case 2: // Pflichtfelder fehlen
                        if ($translationStatus[0] == $translationStatus[1]) {
                            $bValid = false;
                        }
                        break;
                    case 3: // Optionale Felder fehlen
                        if ($translationStatus[2] == $translationStatus[3]) {
                            $bValid = false;
                        }
                        break;
                    case 4: // alles übersetzt
                        if ($translationStatus[0] < $translationStatus[1] ||
                            $translationStatus[2] < $translationStatus[3]) {
                            $bValid = false;
                        }
                        break;
                }

                if ($bValid) {
                    $aReturn[] = $sOxId;
                }

                $rs->moveNext();
            }
        }

        return $aReturn;
    }

    /**
     * @return mixed
     */
    public function getCount()
    {
        $baseSQL = $this->_buildSQL(true);
        $sSQL = $baseSQL;
        // echo($sSQL);
        // echo("<br><br><br>");
        $oDb = \oxDb::getDb();
        $rs = $oDb->Execute($sSQL);

        return $rs->RecordCount();
    }

    /**
     * @param int $nStartRes
     * @param int $nLimitRes
     * @return array
     */
    public function getRows($nStartRes = 0, $nLimitRes = 50)
    {
        $artLanguage = $this->getSourceLanguage();

        $baseSQL = $this->_buildSQL(false, $nStartRes, $nLimitRes);
        $sSQL = $baseSQL;
        //  echo($sSQL . "<br>");
        $oDb = \oxDb::getDb();
        $rs = $oDb->Execute($sSQL);

        $arrObjects = array();
        if ($rs->RecordCount() > 0) {
            while (!$rs->EOF) {
                $sOxId = $rs->fields[0];

                $oContent = oxNew( 'oxContent');
                $oContent->loadInLang($artLanguage, $sOxId);

                $arrObjects[] = $oContent;

                $rs->moveNext();
            }
        }
        return $arrObjects;
    }

    /**
     * @param null $oArticle
     *
     * @return string
     */
    public function getObjectInternalId($oContent = null)
    {
        if ($oContent == null) {
            return "";
        }
        return $oContent->oxcontents__oxid->value;
    }

    /**
     * @param null $oArticle
     *
     * @return string
     */
    public function getObjectId($oContent = null)
    {
        if ($oContent == null) {
            return "";
        }
        return $oContent->oxcontents__oxloadid->value;
    }

    /**
     * @param null $oArticle
     *
     * @return string
     */
    public function getObjectTitle($oContent = null)
    {
        if ($oContent == null) {
            return "no Title";
        }
        return $oContent->oxcontents__oxtitle->value;
    }

    /**
     * @param null $oArticle
     * @return string
     */
    public function getTranslationStatus($contId)
    {

        $newLanguage = $this->getDestLanguage();
        $oldLanguage = $this->getSourceLanguage();

        $aReturnStatus = array();

        $oContentSrc = oxNew( 'oxContent');
        $oContentSrc->loadInLang($oldLanguage, $contId);
        $oContentDest = oxNew( 'oxContent');
        $oContentDest->loadInLang($newLanguage, $contId);

        if ($oContentSrc != null && $newLanguage != null) {
            $nCounterMandatory = 0;
            $nCounterMandatoryTarget = 0;
            $nCounterOptional = 0;
            $nCounterOptionalTarget = 0;


            foreach ($this->_getConfig() as $singleConf) {

                $sSrcValue = $this->getContentValue($singleConf[0], $oContentSrc, $oldLanguage);
                $sCurValue = $this->getContentValue($singleConf[0], $oContentDest, $newLanguage);

                if ($singleConf[1] == "Pflicht") {
                    if ($sCurValue != "") {
                        $nCounterMandatory++;
                    }
                    $nCounterMandatoryTarget++;
                } else {
                    // check, of the source of optional field is set,
                    // if not then the user don't have to translate
                    if ($sSrcValue != "") {
                        if ($sCurValue != "") {
                            $nCounterOptional++;
                        }
                        $nCounterOptionalTarget++;
                    }

                }
            }

            $aReturnStatus[0] = $nCounterMandatory;
            $aReturnStatus[1] = $nCounterMandatoryTarget;
            $aReturnStatus[2] = $nCounterOptional;
            $aReturnStatus[3] = $nCounterOptionalTarget;
        }
        return $aReturnStatus;
    }

    /**
     * @param $objId
     * @param $confDet
     */
    public function getHTMLValue ($objId, $confDet) {
        $oContentSrc = oxNew( 'oxContent');
        $oContentSrc->loadInLang($this->getSourceLanguage(), $objId);

        $arrConfig = $this->_getConfig();
        $sReturn = $this->getContentValue($arrConfig[$confDet][0], $oContentSrc,
            $this->getSourceLanguage());

        echo($sReturn);
    }

    /**
     * @param $confType
     * @param $oArticle
     * @param $sLang
     * @return string
     */
    public function getContentValue($confType, $oContent, $sLang)
    {

        switch ($confType) {
        case "Titel" :
            return $oContent->oxcontents__oxtitle->value;
            break;

        case "Langbeschreibung" :
            return $oContent->oxcontents__oxcontent->value;
            break;

        case "SEO Keywords" :
            $oContentSeo = oxnew("oxSeoEncoderContent");
            return $oContentSeo->getMetaData($oContent->oxcontents__oxid->value, "oxkeywords", null, $sLang, null);
            break;
        case "SEO Description" :
            $oContentSeo = oxnew("oxSeoEncoderContent");
            return $oContentSeo->getMetaData($oContent->oxcontents__oxid->value, "oxdescription", null, $sLang, null);
            break;

        }

        return "";
    }

    /**
     * @param $confType
     * @param $oArticle
     * @param $newValue
     * @param $aParamsTemp
     * @return mixed
     */
    public function setContentValue($confType, $oContent, $newValue, $aParamsTemp)
    {
        switch ($confType) {
        case "Titel" :
            $aParamsTemp['oxcontents__oxtitle'] = trim($newValue);
            break;

        case "Langbeschreibung" :
            $aParamsTemp['oxcontents__oxcontent'] = $this->_processLongDesc( trim($newValue) );
            $aParamsTemp['oxcontents__oxcontent']  = str_replace('-&gt;', '->', $aParamsTemp['oxcontents__oxcontent']);
            break;

        case "SEO Keywords" :
            $strLangDest = $this->getDestLanguage();
            if ($strLangDest == "") $strLangDest = "-1";
            $oContentSeo = oxnew("oxSeoEncoderContent");
            $sOxid = $oContent->oxcontents__oxid->value;
            $this->saveSeoData($oContentSeo, $sOxid,
                \oxRegistry::getConfig()->getShopId(), $strLangDest, $newValue, "oxkeywords");

            break;
        case "SEO Description" :
            $strLangDest = $this->getDestLanguage();
            if ($strLangDest == "") $strLangDest = "-1";
            $oContentSeo = oxnew("oxSeoEncoderContent");
            $sOxid = $oContent->oxcontents__oxid->value;
            $this->saveSeoData($oContentSeo, $sOxid,
                \oxRegistry::getConfig()->getShopId(), $strLangDest, $newValue, "oxdescription");

            break;
        }

        return $aParamsTemp;
    }

    function saveSeoData($oSeoObject, $objectid, $shopid, $iLang, $newValue, $sType) {
        try {
            $oDb = \oxDb::getDb();
            $objectid = $oDb->quote($objectid);
            $shopid = $oDb->quote($shopid);


            $oStr = getStr();
            if ( $newValue !== false ) {
                $newValue = $oDb->quote( $oStr->htmlspecialchars(
                    $oSeoObject->encodeString( $oStr->strip_tags( $newValue ),
                        false, $iLang )
                ) );
            }

            $sQ = "insert into oxobject2seodata
                           ( oxobjectid, oxshopid, oxlang, $sType )
                       values
                           ( {$objectid}, {$shopid}, {$iLang}, ".( $newValue ? $newValue  : "''" )." )
                       on duplicate key update
                           $sType = ".( $newValue ? $newValue : "$sType" )." ";

            $oDb->execute( $sQ );
        } catch (Exception $e) {
            $aReturn['status'] = 1;
            $aReturn['result'] = $e;

            echo(json_encode($aReturn));
            die();

        }
    }

    /**
     * @param $sValue
     * @return mixed
     */
    public function _processLongDesc ($sValue) {
        $sValue = str_replace( '&amp;nbsp;', '&nbsp;', $sValue );
        $sValue = str_replace( '&amp;', '&', $sValue );
        $sValue = str_replace( '&quot;', '"', $sValue );
        $sValue = str_replace( '&lang=', '&amp;lang=', $sValue);
        $sValue = str_replace( '<p>&nbsp;</p>', '', $sValue);
        $sValue = str_replace( '<p>&nbsp; </p>', '', $sValue);
        $sValue = str_replace( '-&gt;', '->', $sValue);
        return $sValue;
    }

    /**
     * @param $searchObjectId
     * @param $sLangDest
     * @param $aValues
     */
    public function save($searchObjectId, $sLangDest, $aValues)
    {
        $oContentDest = oxNew( 'oxContent');
        $oContentDest->loadInLang($sLangDest, $searchObjectId);

        $aParams = array();

        $aConfig = $this->_getConfig();
        if (count($aConfig) != count($aValues)) {
            $aReturn['status'] = 1;
            // ToDo: Mehrsprachigkeit
            $aReturn['result'] = "Wrong number of fields";
            return $aReturn;
        }

        for ($i = 0; $i < count($aConfig); $i++) {
            $aParams = $this->setContentValue(
                $aConfig[$i][0], $oContentDest, $aValues[$i], $aParams
            );
        }
        $oContentDest->assign($aParams);

        try {
            $oContentDest->save();
        } catch (Exception $e) {
            $aReturn['status'] = 1;
            $aReturn['result'] = $e;

            return $aReturn;

        }


        $aReturn['status'] = 0;
        $aReturn['result'] = "OK";

        return $aReturn;
    }

    /**
     * @param $searchObjectId
     * @param $sLangSrc
     * @param $sLangDest
     */
    public function getCompleteObject($searchObjectId, $sLangSrc, $sLangDest)
    {
        $oContentSrc = oxNew( 'oxContent');
        $oContentSrc->loadInLang($sLangSrc, $searchObjectId);
        $oContentDest = oxNew( 'oxContent');
        $oContentDest->loadInLang($sLangDest, $searchObjectId);

        $pluginLanguages = $this->getLangList();

        $sLangSrcName = "";
        $sLangDestName = "";
        foreach ($pluginLanguages as $lang) {
            if ($lang['id'] == $sLangSrc) {
                $sLangSrcName = $lang['name']; break;
            }
        }
        foreach ($pluginLanguages as $lang) {
            if ($lang['id'] == $sLangDest) {
                $sLangDestName = $lang['name']; break;
            }
        }

        $aReturnValues = array();

        $aLanguages = array();
        $singleLang = array();
        $singleLang['id'] = $sLangSrc;
        $singleLang['name'] = $sLangSrcName;
        $aLanguages['source'] = $singleLang;

        $singleLang = array();
        $singleLang['id'] = $sLangDest;
        $singleLang['name'] = $sLangDestName;
        $aLanguages['destination'] = $singleLang;

        $aReturnValues['languages'] = $aLanguages;

        $aValues = array();

        foreach ($this->_getConfig() as $singleConf) {
            $singleValue = array();

            $strContentSrc = $this->getContentValue($singleConf[0], $oContentSrc, $sLangSrc);
            $strContentDest = $this->getContentValue($singleConf[0], $oContentDest, $sLangDest);

            // $strContentDest = htmlentities($strContentDest, ENT_QUOTES, "UTF-8");

            $inputType = strtolower($singleConf[2]);
            $inputMaxLength = $singleConf[3];

            $singleValue['confName'] = $singleConf[0];
            $singleValue['confType'] = $inputType;
            $singleValue['confMaxLength'] = $inputMaxLength;
            $singleValue['confContSrc'] = $strContentSrc;
            $singleValue['confContDest'] = $strContentDest;

            $aValues[] = $singleValue;
        }
        $aReturnValues['values'] = $aValues;
        /*
        echo("<pre>");
        print_r($aReturnValues);
        echo("</pre>");
        */
        return $aReturnValues;
    }


    /**
     *
     */
    public function addSpecialHeader()
    {
        $html = '';
        $html .= '<script src="plugins/oxid5/js/oxid5.js"></script>';
        echo($html);
    }

    /**
     * @return array
     */
    public function getShopList () {
        $sDefShop = \oxRegistry::getConfig()->getShopConfVar("sDefaultShop");

        if($sDefShop == null){
            $sDefShop = 1;
        }

        if ($this->getShopID() == null) {
            $this->setShopID("1");
        }
        $aShopReturn = array();
        $pluginShop = $this->getShopID();

        $sSelect = "select oxid, oxname ";
        $sFrom = "from oxshops ";
        $sWhere = "where 1 ";
        $sWhere .= "and oxactive = 1 ";

        $sSQL = $sSelect . $sFrom . $sWhere;

        $oDb = \oxDb::getDb();
        $rs = $oDb->Execute($sSQL);

        if ($rs->RecordCount() > 0) {
            while (!$rs->EOF) {
                $sOxId = $rs->fields[0];
                $sOXNAME = $rs->fields[1];
                $aShops[] = array("id" => $sOxId, "shopName" => $sOXNAME);

                $rs->MoveNext();
            }
        }

        foreach ($aShops as $shop) {
            $aTemp = array();
            $aTemp['id'] = $shop['id'];
            $aTemp['name'] = $shop['shopName'];
            $bSelected = false;

//            echo "<pre>";
//            echo $shop['id']."->".filter_input(INPUT_GET, "shopID")."<br>";



            if ($shop['id'] == filter_input(INPUT_GET, "shopID") ) {
                $bSelected = true;
            }

            if ($bSelected) {
                $aTemp['selected'] = 1;
            } else {
                $aTemp['selected'] = 0;
            }
            $aShopReturn[] = $aTemp;
        }
        return $aShopReturn;
    }

    /**
     * @return array
     */
    public function getFilterView()
    {
        $currentFilter = $this->getFilter();

        $retFilter = array();

        // Checkbox "aktive Produkte"
        $filter = new Filter();
        $filter->setTitle('nur aktive CMS');
        $filter->setType('checkbox');
        $filter->setName('filter_active');
        $filter->setValue(1);
        $filter->setCheckedValue($currentFilter['filter_active']);

        $retFilter[] = $filter;

        // TextInput "Produktname"
        $filter = new Filter();
        $filter->setTitle('Suche nach');
        $filter->setType('inputText');
        $filter->setName('filter_titlesearch');
        $filter->setValue("");
        $filter->setCheckedValue($currentFilter['filter_titlesearch']);

        $retFilter[] = $filter;

        // Selectbox "CMS-Ordner"
        $aValues = array();
        $aFolder = \oxRegistry::getConfig()->getConfigParam('aCMSfolder');
        $keys = array_keys($aFolder);
        foreach ($keys as $oFolder) {
            $sValFolder = str_replace("CMSFOLDER_", "", $oFolder);
            $aValues[] = array($oFolder, $sValFolder );
        }

        $filter = new Filter();
        $filter->setTitle('CMS-Ordner');
        $filter->setType('select');
        $filter->setName('filter_folder');
        $filter->setValue($aValues);
        $filter->setSelection($currentFilter['filter_folder']);

        $retFilter[] = $filter;


        // Select "Produktstatus"
        $aValues = array();
        $aValues[] = array("1", "nicht alles übersetzt");
        $aValues[] = array("2", "fehlende Pflichtfelder");
        $aValues[] = array("3", "fehlende Optionale Felder");
        $aValues[] = array("4", "alles übersetzt");

        $filter = new Filter();
        $filter->setTitle('Übersetzungsstatus');
        $filter->setType('select');
        $filter->setName('filter_translationstatus');
        $filter->setValue($aValues);
        $filter->setSelection($currentFilter['filter_translationstatus']);

        $retFilter[] = $filter;

        return $retFilter;
    }

    /**
     * @return array
     */
    public function getLangList () {
        $sDefLang = \oxRegistry::getConfig()->getShopConfVar("sDefaultLang");

        if ($this->getSourceLanguage() == null) {
            $this->setSourceLanguage($sDefLang);
        }
        $aLangReturn = array();
        $pluginLang = $this->getLanguages();
        foreach ($pluginLang as $lang) {
            $aTemp = array();
            $aTemp['id'] = $lang->getId();
            $aTemp['name'] = $lang->getName();
            $bSelected = false;
            if ($lang->getId() == $this->getSourceLanguage() ) {
                $bSelected = true;
            }

            if ($bSelected) {
                $aTemp['selected'] = 1;
            } else {
                $aTemp['selected'] = 0;
            }
            $aLangReturn[] = $aTemp;
        }
        return $aLangReturn;
    }


    /**
     * @param int $nStart
     * @param int $nLimit
     * @return array
     */
    public function getResults($nStart = 0, $nLimit = 50) {
        $strLangDest = $this->getDestLanguage();
        if ($strLangDest == "") $strLangDest = "-1";

        $pluginRows = $this->getRows($nStart, $nLimit);
        $aObjectInfo = array();

        foreach ($pluginRows as $pluginObject) {
            $aTranslationStatus = $this->getTranslationStatus($this->getObjectInternalId($pluginObject));

            $translObj = new Translation();
            $translObj->setInternalId( $this->getObjectInternalId($pluginObject) );
            $translObj->setExternalId( $this->getObjectId($pluginObject) );
            $translObj->setObjectTitle( $this->getObjectTitle($pluginObject) );
            $translObj->setTranslationStatus($aTranslationStatus);

            $aObjectInfo[] = $translObj;
        }


        return $aObjectInfo;
    }


    /**
     * @return array
     */
    public function getSummary()
    {
        $strLangDest = $this->getDestLanguage();
        $pluginRows = $this->getRows(0, "-1");
        $aObjectInfo = array();

        foreach ($pluginRows as $pluginObject) {
            $aTranslationStatus = $this->getTranslationStatus($this->getObjectInternalId($pluginObject));

            $translObj = new Translation();
            $translObj->setInternalId( $this->getObjectInternalId($pluginObject) );
            $translObj->setExternalId( $this->getObjectId($pluginObject) );
            $translObj->setObjectTitle( $this->getObjectTitle($pluginObject) );
            $translObj->setTranslationStatus($aTranslationStatus);

            $aObjectInfo[] = $translObj;
        }


        $nAnzProductsMandatoryDone = 0;
        $nAnzProductsOptionalDone = 0;
        $nAnzProductsCompleteDone = 0;

        $nSumProductsMandatoryDone = 0;
        $nSumProductsMandatoryTarget = 0;
        $nSumProductsOptionalDone = 0;
        $nSumProductsOptionalTarget = 0;

        $nMaxProducts = 0;
        foreach ($aObjectInfo as $aProduct) {
            $aTranslStatObj = $aProduct->getTranslationStatus();

            $nAnzMandatoryDone      = $aTranslStatObj[0];
            $nAnzMandatoryTarget    = $aTranslStatObj[1];
            $nAnzOptionalDone       = $aTranslStatObj[2];
            $nAnzOptionalTarget     = $aTranslStatObj[3];


            $nSumProductsMandatoryDone += $nAnzMandatoryDone;
            $nSumProductsMandatoryTarget += $nAnzMandatoryTarget;
            $nSumProductsOptionalDone += $nAnzOptionalDone;
            $nSumProductsOptionalTarget += $nAnzOptionalTarget;

            $bMandatoryDone = false;
            $bOptionalDone = false;
            if ($nAnzMandatoryDone == $nAnzMandatoryTarget) {
                $nAnzProductsMandatoryDone++;
                $bMandatoryDone = true;
            }

            if ($nAnzOptionalDone == $nAnzOptionalTarget) {
                $nAnzProductsOptionalDone++;
                $bOptionalDone = true;
            }
            if ($bMandatoryDone && $bOptionalDone) {
                $nAnzProductsCompleteDone++;
            }
            $nMaxProducts++;
        }

        $aReturn = array();
        if ($strLangDest != "") {
            $aReturn[] = array('CMS-Seiten (Pflichtfelder komplett)',
                $nAnzProductsMandatoryDone, $nMaxProducts);
            $aReturn[] = array('CMS-Seiten (Optionalfelder komplett)',
                $nAnzProductsOptionalDone, $nMaxProducts);
            $aReturn[] = array('CMS-Seiten komplett fertig',
                $nAnzProductsCompleteDone, $nMaxProducts);
            $aReturn[] = array('Pflichtfelder',
                $nSumProductsMandatoryDone, $nSumProductsMandatoryTarget);
            $aReturn[] = array('optionale Felder',
                $nSumProductsOptionalDone, $nSumProductsOptionalTarget);
        }
        return $aReturn;
    }


}
?>
