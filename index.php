<?php

/**
 * gn2 :: Baerbelfish
 *
 * PHP version 5
 *
 * @category gn2 :: Baerbelfish
 * @package  gn2 :: Baerbelfish
 * @author   Dave Holloway <dh@gn2-netwerk.de>
 * @author   Kristian Berger <kb@gn2-netwerk.de>
 * @license  GN2 Commercial Addon License http://www.gn2-netwerk.de/
 * @version  GIT: <git_id>
 * @link     http://www.gn2-netwerk.de/
 */
namespace gn2\Baerbelfish\Core;
define("BAERBELFISH", 1);
define("BAERBELFISH_ERROR_REPORTING", E_ALL ^E_NOTICE ^E_STRICT);

call_user_func(
    function () {
        error_reporting(BAERBELFISH_ERROR_REPORTING);
        ini_set('display_errors', 1);

        header('Content-Type: text/html; charset=utf-8');
        ob_start();
        require_once "vendor/autoload.php";

        $strPluginId = $_REQUEST['plugin'];
        $functionActive = $_REQUEST['function'];

        if (isset($_REQUEST['shopID'])) {
            $strShopID = $_REQUEST['shopID'];
        }

        if (isset($_REQUEST['langSource'])) {
            $strLanguageSource = $_REQUEST['langSource'];
        }
        if (isset($_REQUEST['langDest'])) {
            $strLanguageDest = $_REQUEST['langDest'];
        }
        if (isset($_REQUEST['curFunc'])) {
            $currentFunc = $_REQUEST['curFunc'];
        }

        /* Load Plugins */
        $plugins = array();
        foreach (glob(__DIR__."/plugins/*", GLOB_ONLYDIR) as $pluginDir) {
            $basePlugin = basename($pluginDir);
            foreach (glob($pluginDir."/plugin.*") as $pluginFile) {
                $baseFile = basename($pluginFile);
                $pluginData = explode('.', $baseFile);
                if (count($pluginData) == 3) {
                    $class = "gn2\\Baerbelfish\\Plugin\\".$basePlugin."_".$pluginData[1];
                    include_once $pluginFile;
                    if (class_exists($class)) {
                        $plugin = new $class;
                        if ($plugin instanceof \gn2\Baerbelfish\Core\Plugin) {
                            if ($plugin->isActive()) {
                                $plugins[] = $plugin;
                            }
                        }
                    }
                }
            }
        }

        $objPluginActive = null;

        $controller = 'Start';
        if ($strPluginId != "") {
            foreach ($plugins as $objPlugin) {
                if ($objPlugin->getId() == $strPluginId) {
                    $objPluginActive = $objPlugin; break;
                }
            }
        }

        if ($objPluginActive) {
            $controller = 'PluginList';
        }

        if ($functionActive == "editobj") {
            $controller = 'PluginEdit';
        }

        if ($functionActive == "allProp") {
            $controller = 'PluginEditAllIVProp';
        }

        if ($functionActive == "allComp") {
            $controller = 'PluginEditAllIVComp';
        }

        if ($functionActive == "allPropVal") {
            $controller = 'PluginEditAllIVPropVal';
        }

        if ($functionActive == "showhtml") {
            $controller = 'PluginHTML';
        }
        switch ($currentFunc) {
        case "getCompleteObject" :
            if ($objPluginActive != null) {
                $objId = $_REQUEST['objId'];
                $objPluginActive->getCompleteObject(
                    $objId, $strLanguageSource, $strLanguageDest
                );
            }
            die();
            break;
        case "saveCompleteObject" :
            if ($objPluginActive != null) {
                $objId = $_REQUEST['objId'];
                $nCountVal = $_REQUEST['newValues_count'];
                $aValues = array();
                for ($i = 0; $i < $nCountVal; $i++) {
                    $aValues[] = $_REQUEST['newValues_' . $i];
                }
                $objPluginActive->saveCompleteObject(
                    $objId, $strLanguageDest, $aValues
                );
            }
            die();
            break;
        case "showResults" :
            $nCountVal = $_REQUEST['filter_count'];
            $aFilter = array();
            for ($i = 0; $i < $nCountVal; $i++) {
                $aFilter[] = $_REQUEST['filter_' . $i];
            }

            $objPluginActive->showResults($strShopID, $strLanguageSource, $strLanguageDest, $aFilter);
            die();
            break;
        }
        $loader = new \Twig_Loader_Filesystem('src/views/');
        $twig = new \Twig_Environment($loader, array(
            'cache' => 'cache',
            'debug' => 'true',
        ));
        $twig->addExtension(new \Twig_Extension_Debug());
        $controller = "gn2\\Baerbelfish\\Controller\\".$controller;
        $controller = new $controller($plugins, $objPluginActive);
        $controller->setView($twig);
        $controller->display();
    }
);

